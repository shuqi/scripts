import sys
from array import array
from bisect import bisect_left

import ROOT as rt

# variables and binning definition
binning = {"ctheta_CS": [-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1],
           # "phi_cs" : [0, 1.05, 2.1, 3.15, 4.2, 5.25, 6.3]
           "phi_CS": [0, 0.7875, 1.575, 2.3625, 3.15, 3.9375, 4.725, 5.5125, 6.3]
           # "phi_cs" : [0, 0.63, 1.26, 1.89, 2.52, 3.15, 3.78, 4.41, 5.04, 5.67, 6.3]
           }
Zpt_binning = [0, 35, 60, 90, 135, 2500]
Zy_binning = [0, 0.75, 1.5, 2.5]
nBins = (len(Zpt_binning)-1)*(len(binning["ctheta_CS"])-1)
nZyBins = len(Zy_binning) - 1
variables = ["ctheta_CS", "phi_CS"]
# JetSyst no nominal
systematics = [
               "MUON_MS__1up",
               "MUON_MS__1down",
               "MUON_ID__1up",
               "MUON_ID__1down",
               "MUON_SCALE__1up",
               "MUON_SCALE__1down",
               "MUON_SAGITTA_RHO__1up",
               "MUON_SAGITTA_RHO__1down",
               "MUON_EFF_ISO_SYS__1up",
               "MUON_EFF_ISO_SYS__1down",
               "MUON_EFF_RECO_SYS__1up",
               "MUON_EFF_RECO_SYS__1down",
               "MUON_EFF_RECO_STAT__1up",
               "MUON_EFF_RECO_STAT__1down",
               "MUON_EFF_ISO_STAT__1up",
               "MUON_EFF_ISO_STAT__1down",
               "MUON_EFF_TTVA_SYS__1up",
               "MUON_EFF_TTVA_SYS__1down",
               "MUON_EFF_TTVA_STAT__1up",
               "MUON_EFF_TTVA_STAT__1down",
               "MUON_SAGITTA_RESBIAS__1up",
               "MUON_SAGITTA_RESBIAS__1down",
               "MUON_EFF_RECO_SYS_LOWPT__1up",
               "MUON_EFF_RECO_SYS_LOWPT__1down",
               "MUON_EFF_RECO_STAT_LOWPT__1up",
               "MUON_EFF_RECO_STAT_LOWPT__1down",
               "PRW_DATASF__1up",
	       "PRW_DATASF__1down",
	       "PH_EFF_ISO_Uncertainty__1up",
               "PH_EFF_ISO_Uncertainty__1down",
               "PH_EFF_TRIGGER_Uncertainty__1up",
               "PH_EFF_TRIGGER_Uncertainty__1down",
               "PH_EFF_ID_Uncertainty__1up",
               "PH_EFF_ID_Uncertainty__1down",
               "EG_SCALE_ALL__1up",
               "EG_SCALE_ALL__1down",
               "EG_SCALE_AF2__1up",
               "EG_SCALE_AF2__1down",
               "EG_RESOLUTION_ALL__1up",
               "EG_RESOLUTION_ALL__1down",
               "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up",
               "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down",
               "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up",
               "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down",
               "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up",
               "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"
               'JET_JvtEfficiency__1up',
	       'JET_JvtEfficiency__1down',
	       'JET_Pileup_PtTerm__1up',
               'JET_Pileup_PtTerm__1down',
	       'JET_BJES_Response__1up',
	       'JET_BJES_Response__1down',
	       'JET_fJvtEfficiency__1up',
	       'JET_fJvtEfficiency__1down',
               'JET_Flavor_Response__1up',
	       'JET_Flavor_Response__1down',
	       'JET_Pileup_OffsetMu__1up',
	       'JET_Pileup_OffsetMu__1down',
	       'JET_Pileup_OffsetNPV__1up',
	       'JET_Pileup_OffsetNPV__1down',
	       'JET_JER_EffectiveNP_3__1up',
	       'JET_JER_EffectiveNP_3__1down',
	       'JET_JER_DataVsMC_MC16__1up',
	       'JET_JER_DataVsMC_MC16__1down',
	       'JET_PunchThrough_MC16__1up',
	       'JET_PunchThrough_MC16__1down',
	       'JET_JER_EffectiveNP_7__1up',
               'JET_JER_EffectiveNP_7__1down',
	       'JET_JER_EffectiveNP_9__1up',
	       'JET_JER_EffectiveNP_9__1down',
	       'JET_JER_EffectiveNP_6__1up',
	       'JET_JER_EffectiveNP_6__1down',
	       'JET_JER_EffectiveNP_8__1up',
	       'JET_JER_EffectiveNP_8__1down',
	       'JET_JER_EffectiveNP_4__1up',
	       'JET_JER_EffectiveNP_4__1down',
	       'JET_JER_EffectiveNP_5__1up',
	       'JET_JER_EffectiveNP_5__1down',
	       'JET_JER_EffectiveNP_2__1up',
	       'JET_JER_EffectiveNP_2__1down',
	       'JET_JER_EffectiveNP_1__1up',
	       'JET_JER_EffectiveNP_1__1down',
	       'JET_EffectiveNP_Mixed2__1up',
	       'JET_EffectiveNP_Mixed2__1down',
	       'JET_EffectiveNP_Mixed1__1up',
	       'JET_EffectiveNP_Mixed1__1down',
	       'JET_EffectiveNP_Mixed3__1up',
	       'JET_EffectiveNP_Mixed3__1down',
	       'JET_JER_EffectiveNP_11__1up',
	       'JET_JER_EffectiveNP_11__1down',
	       'JET_JER_EffectiveNP_10__1up',
	       'JET_JER_EffectiveNP_10__1down',
	       'JET_Pileup_RhoTopology__1up',
	       'JET_Pileup_RhoTopology__1down',
	       'JET_Flavor_Composition__1up',
	       'JET_Flavor_Composition__1down',
	       'JET_EffectiveNP_Detector2__1up',
	       'JET_EffectiveNP_Detector2__1down',
	       'JET_EffectiveNP_Detector1__1up',
	       'JET_EffectiveNP_Detector1__1down',
	       'JET_SingleParticle_HighPt__1up',
	       'JET_SingleParticle_HighPt__1down',
	       'JET_EffectiveNP_Modelling1__1up',
	       'JET_EffectiveNP_Modelling1__1down',
	       'JET_EffectiveNP_Modelling2__1up',
	       'JET_EffectiveNP_Modelling2__1down',
	       'JET_EffectiveNP_Modelling3__1up',
	       'JET_EffectiveNP_Modelling3__1down',
	       'JET_EffectiveNP_Modelling4__1up',
	       'JET_EffectiveNP_Modelling4__1down',
	       'JET_EffectiveNP_Statistical1__1up',
	       'JET_EffectiveNP_Statistical1__1down',
	       'JET_EffectiveNP_Statistical2__1up',
	       'JET_EffectiveNP_Statistical2__1down',
	       'JET_EffectiveNP_Statistical3__1up',
	       'JET_EffectiveNP_Statistical3__1down',
	       'JET_EffectiveNP_Statistical4__1up',
	       'JET_EffectiveNP_Statistical4__1down',
	       'JET_EffectiveNP_Statistical5__1up',
	       'JET_EffectiveNP_Statistical5__1down',
	       'JET_EffectiveNP_Statistical6__1up',
	       'JET_EffectiveNP_Statistical6__1down',
	       'JET_JER_EffectiveNP_12restTerm__1up',
	       'JET_JER_EffectiveNP_12restTerm__1down',
	       'JET_EtaIntercalibration_TotalStat__1up',
	       'JET_EtaIntercalibration_TotalStat__1down',
	       'JET_EtaIntercalibration_Modelling__1up',
	       'JET_EtaIntercalibration_Modelling__1down',
	       'JET_EtaIntercalibration_NonClosure_highE__1up',
	       'JET_EtaIntercalibration_NonClosure_highE__1down',
	       'JET_EtaIntercalibration_NonClosure_negEta__1up',
	       'JET_EtaIntercalibration_NonClosure_negEta__1down',
	       'JET_EtaIntercalibration_NonClosure_posEta__1up',
	       'JET_EtaIntercalibration_NonClosure_posEta__1down',
	       'JET_EtaIntercalibration_NonClosure_2018data__1up',
	       'JET_EtaIntercalibration_NonClosure_2018data__1down'
	       ]

path = '/afs/cern.ch/user/s/shuqi/eos/Zy_signal_MC_Sh2211'
dsid = 'mc16e_eegamma_JetSyst'
#dsid = 'mc16a_eegamma'
sh = rt.SH.SampleHandler()
rt.SH.ScanDir().sampleDepth(0).scan(sh, path+'/'+'CS_Info_sig_'+dsid)
sh.printContent()

samples = []
print("num.samples = ", sh.size())

#for iter in range(0, 3):
for iter in range(0, sh.size()):
    sample = sh.at(iter)
    print sample.name()

    inputFile = rt.TFile.Open(path+'/'+'CS_Info_sig_'+dsid+'/'+sample.name(), 'READONLY')
    outputFile = rt.TFile.Open(path+'/'+'UnfoldHist/'+dsid+'/'+sample.name(), 'RECREATE')

    # start looping over systematics
    for syst in systematics:
        print("Processing systematic {}".format(syst))
        # first check whether it exists
        if not hasattr(inputFile, syst):
            print("TTree {} is not found in file {}".format(syst, sample.name()))
            continue
        # prepare output dir
        dir_syst = outputFile.mkdir(syst)
        dir_syst.cd()

        # prepare histograms to be filled
        h_migration_matrice = {}
        h_fiducial = {}
        h_reconstructed = {}
        h_effcorrnum = {}
        h_effcorrden = {}
        h_fidcorrnum = {}
        h_fidcorrden = {}
        for var in variables:
            print("create histograms for variable {}".format(var))
            h_migration_matrice[var] = {}
            h_fidcorrnum[var] = {}
            h_effcorrden[var] = {}
            h_fidcorrden[var] = {}
            h_effcorrnum[var] = {}
            h_fiducial[var] = {}
            h_reconstructed[var] = {}
            for iZyBin in range(1, nZyBins+1):
                h_migration_matrice[var][iZyBin-1] = rt.TH2D("sumBackground_migration_matrix_"+var+"_SliceZy"+str(iZyBin),
                                                         "sumBackground_"+syst+"_migration_matrix_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins, nBins, 0, nBins)
                h_fidcorrnum[var][iZyBin-1] = rt.TH1D("sumBackground_fidcorrnum_"+var+"_SliceZy" +
                                                  str(iZyBin), "sumBackground_"+syst+"_fidcorrnum_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)
                h_effcorrden[var][iZyBin-1] = rt.TH1D("sumBackground_effcorrden_"+var+"_SliceZy" +
                                                  str(iZyBin), "sumBackground_"+syst+"_effcorrden_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)
                h_fidcorrden[var][iZyBin-1] = rt.TH1D("sumBackground_fidcorrden_"+var+"_SliceZy" +
                                                  str(iZyBin), "sumBackground_"+syst+"_fidcorrden_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)
                h_effcorrnum[var][iZyBin-1] = rt.TH1D("sumBackground_effcorrnum_"+var+"_SliceZy" +
                                                  str(iZyBin), "sumBackground_"+syst+"_effcorrnum_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)
                h_fiducial[var][iZyBin-1] = rt.TH1D("sumBackground_truth_"+var+"_SliceZy"+str(iZyBin),
                                                "sumBackground_"+syst+"_truth_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)
                h_reconstructed[var][iZyBin-1] = rt.TH1D("sumBackground_A_"+var+"_SliceZy" +
                                                     str(iZyBin), "sumBackground_"+syst+"_A_"+var+"_SliceZy"+str(iZyBin), nBins, 0, nBins)

        # start looping over tree
        tree = inputFile.Get(syst)
        for event in tree:
            PassReco = bool(getattr(event, "passReconstructed"))
            PassFid = bool(getattr(event, "passFiducial"))

            # NOTE for the migration matrix, only need events passing both reco and fiducial cuts
            # NOTE apply the cut here to speed up
            # NOTE remove it if filling other histograms that require different selections

            # get event weight
            reco_w = getattr(event, "weight")
            true_w = getattr(event, "truth_weight")

            # determine Zy bin
            abs_reco_Zy = abs(getattr(event, "Ry_ll"))
            abs_true_Zy = abs(getattr(event, "truth_Ry_ll"))
            reco_bin_Zy = bisect_left(Zy_binning, abs_reco_Zy)
            true_bin_Zy = bisect_left(Zy_binning, abs_true_Zy)
            # determine Zpt bin first
            reco_pt_Z = getattr(event, "pt_ll")
            true_pt_Z = getattr(event, "truth_pt_ll")
            reco_bin_Zpt = bisect_left(Zpt_binning, reco_pt_Z)
            true_bin_Zpt = bisect_left(Zpt_binning, true_pt_Z)
            if reco_bin_Zpt == len(Zpt_binning):
                reco_bin_Zpt = len(Zpt_binning) - 1

            if true_bin_Zpt == len(Zpt_binning):
                true_bin_Zpt = len(Zpt_binning) - 1

            # for each variable
            for var in variables:
                reco_var = getattr(event, var)
                true_var = getattr(event, "truth_"+var)

                # determine variable bin
                reco_var_bin = bisect_left(binning[var], reco_var)
                true_var_bin = bisect_left(binning[var], true_var)

                # NOTE do not need to fix binning for angular variables, they are in the range

                # calculate bin in the histogram
                # NOTE returned result from bisect_lef function start from 1 not 0!
                # bin = nbin_var * bin_pt_Z + bin_var
                reco_bin = (len(binning[var])-1) * \
                    (reco_bin_Zpt - 1) + (reco_var_bin - 1)
                true_bin = (len(binning[var])-1) * \
                     (true_bin_Zpt - 1) + (true_var_bin - 1)

                # fill the histogram
                if PassReco and PassFid and reco_bin_Zy == true_bin_Zy:
                    h_migration_matrice[var][reco_bin_Zy-1].Fill(reco_bin, true_bin, reco_w)
                    h_fidcorrnum[var][reco_bin_Zy-1].Fill(reco_bin, reco_w)
                    h_effcorrnum[var][reco_bin_Zy-1].Fill(true_bin, reco_w)
                
		if PassFid:
                    h_fiducial[var][true_bin_Zy-1].Fill(true_bin, true_w)
                    h_effcorrden[var][true_bin_Zy-1].Fill(true_bin, true_w)

                if PassReco:
                    h_reconstructed[var][reco_bin_Zy-1].Fill(reco_bin, reco_w)
                    h_fidcorrden[var][reco_bin_Zy-1].Fill(reco_bin, reco_w)

        for var in variables:
            # sum sliced histograms to get a integrated histogram
            h_migration_sum = h_migration_matrice[var][0].Clone("sumBackground_migration_matrix_"+var)
            h_migration_sum.SetTitle("sumBackground_"+syst+"_migration_matrix_"+var)
            
	    h_fiducial_sum = h_fiducial[var][0].Clone("sumBackground_truth_"+var)
            h_fiducial_sum.SetTitle("sumBackground_"+syst+"_truth_"+var)
            
	    h_reconstructed_sum = h_reconstructed[var][0].Clone("sumBackground_A_"+var)
            h_reconstructed_sum.SetTitle("sumBackground_"+syst+"_A_"+var)
        
	    h_fidcorrnum_sum = h_fidcorrnum[var][0].Clone("sumBackground_fidcorrnum_"+var)
            h_fidcorrnum_sum.SetTitle("sumBackground_"+syst+"_fidcorrnum_"+var)
            
	    h_fidcorrden_sum = h_fidcorrden[var][0].Clone("sumBackground_fidcorrden_"+var)
            h_fidcorrden_sum.SetTitle("sumBackground_"+syst+"_fidcorrden_"+var)
        
	    h_effcorrden_sum = h_effcorrden[var][0].Clone("sumBackground_effcorrden_"+var)
            h_effcorrden_sum.SetTitle("sumBackground_"+syst+"_effcorrden_"+var)
        
	    h_effcorrnum_sum = h_effcorrnum[var][0].Clone("sumBackground_effcorrnum_"+var)
            h_effcorrnum_sum.SetTitle("sumBackground_"+syst+"_effcorrnum_"+var)
        

            for iZyBin in range(nZyBins):
                if iZyBin > 0:
                    h_migration_sum.Add(h_migration_matrice[var][iZyBin])
                    h_fiducial_sum.Add(h_fiducial[var][iZyBin])
                    h_reconstructed_sum.Add(h_reconstructed[var][iZyBin])
                    h_effcorrnum_sum.Add(h_effcorrnum[var][iZyBin])
                    h_effcorrden_sum.Add(h_effcorrden[var][iZyBin])
                    h_fidcorrnum_sum.Add(h_fidcorrnum[var][iZyBin])
                    h_fidcorrden_sum.Add(h_fidcorrden[var][iZyBin])
                h_migration_matrice[var][iZyBin].Write()
                h_fiducial[var][iZyBin].Write()
                h_reconstructed[var][iZyBin].Write()
                h_effcorrnum[var][iZyBin].Write()
                h_effcorrden[var][iZyBin].Write()
                h_fidcorrnum[var][iZyBin].Write()
                h_fidcorrden[var][iZyBin].Write()
            h_migration_sum.Write()
            h_fiducial_sum.Write()
            h_reconstructed_sum.Write()
            h_effcorrnum_sum.Write()
            h_effcorrden_sum.Write()
            h_fidcorrnum_sum.Write()
            h_fidcorrden_sum.Write()
            del h_migration_sum, h_fiducial_sum, h_reconstructed_sum, h_effcorrnum_sum, h_effcorrden_sum, h_fidcorrnum_sum, h_fidcorrden_sum
        del h_migration_matrice
        del h_fiducial
        del h_reconstructed
        del h_effcorrden
        del h_effcorrnum
        del h_fidcorrnum
        del h_fidcorrden
        #outputFile.cd()
        print("Finish {}".format(syst))

    print("All done")
    inputFile.Close()
    outputFile.Close()
