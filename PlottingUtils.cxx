#include "PlottingUtils.h"
#include "Utils.h"
#include <TMultiGraph.h>


using namespace HGamFitter ;
using namespace RooFit ;


void PlottingUtils::ATLASLabel ( double x , double y , const char * text , Color_t color ) 
{
	TLatex l ;
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
	l.DrawLatex(x,y,"ATLAS");
	if (text)
	{
		TLatex p; 
		p.SetNDC();
		p.SetTextFont(42);
		p.SetTextColor(color);
		p.DrawLatex(x+delx,y,text);
	}
}


void PlottingUtils::ATLAS_LABEL ( double x , double y , Color_t color ) 
{
	TLatex l;
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	l.DrawLatex(x,y,"ATLAS");
}


TStyle * PlottingUtils::AtlasStyle() 
{
	TStyle * atlasStyle = new TStyle("ATLAS","Atlas style");

	// use plain black on white colors
	Int_t icol=0; // WHITE
	atlasStyle->SetFrameBorderMode(icol);
	atlasStyle->SetFrameFillColor(icol);
	atlasStyle->SetCanvasBorderMode(icol);
	atlasStyle->SetCanvasColor(icol);
	atlasStyle->SetPadBorderMode(icol);
	atlasStyle->SetPadColor(icol);
	atlasStyle->SetStatColor(icol);
	//atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

	// set the paper & margin sizes
	atlasStyle->SetPaperSize(20,26);

	// set margin sizes
	atlasStyle->SetPadTopMargin(0.05);
	atlasStyle->SetPadRightMargin(0.05);
	atlasStyle->SetPadBottomMargin(0.16);
	atlasStyle->SetPadLeftMargin(0.16);

	// set title offsets (for axis label)
	atlasStyle->SetTitleXOffset(1.4);
	atlasStyle->SetTitleYOffset(1.4);

	// use large fonts
	//Int_t font=72; // Helvetica italics
	Int_t font=42; // Helvetica
	Double_t tsize=0.05;
	atlasStyle->SetTextFont(font);

	atlasStyle->SetTextSize(tsize);
	atlasStyle->SetLabelFont(font,"x");
	atlasStyle->SetTitleFont(font,"x");
	atlasStyle->SetLabelFont(font,"y");
	atlasStyle->SetTitleFont(font,"y");
	atlasStyle->SetLabelFont(font,"z");
	atlasStyle->SetTitleFont(font,"z");
  
	atlasStyle->SetLabelSize(tsize,"x");
	atlasStyle->SetTitleSize(tsize,"x");
	atlasStyle->SetLabelSize(tsize,"y");
	atlasStyle->SetTitleSize(tsize,"y");
	atlasStyle->SetLabelSize(tsize,"z");
	atlasStyle->SetTitleSize(tsize,"z");

	// use bold lines and markers
	atlasStyle->SetMarkerStyle(8);
	atlasStyle->SetMarkerSize(1);
	atlasStyle->SetHistLineWidth(2);
	atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

	// get rid of X error bars 
	//atlasStyle->SetErrorX(0.001);
	// get rid of error bar caps
	atlasStyle->SetEndErrorSize(0);

	// do not display any of the standard histogram decorations
	atlasStyle->SetOptTitle(0);
	//atlasStyle->SetOptStat(1111);
	atlasStyle->SetOptStat(0);
	//atlasStyle->SetOptFit(1111);
	atlasStyle->SetOptFit(0);

	// put tick marks on top and RHS of plots
	atlasStyle->SetPadTickX(1);
	atlasStyle->SetPadTickY(1);

	return atlasStyle;
}


void PlottingUtils::SetAtlasStyle ()
{
	static TStyle * atlasStyle = 0 ;
	Utils::Info ( "PlottingUtils::SetAtlasStyle()" , "Applying ATLAS style settings" ) ;
	if ( atlasStyle == 0 ) atlasStyle = PlottingUtils::AtlasStyle () ;
	gROOT -> SetStyle ( "ATLAS" ) ;
	gROOT -> ForceStyle () ;
}


void PlottingUtils::set2DSignalColorPalette ()
{
	const Int_t NRGBs = 2 ;
	const Int_t NCont = 255 ;

	Double_t stops[NRGBs] = { 0.00 , 1.00 } ;
	Double_t red[NRGBs]   = { 1.00 , 1.00 } ;
	Double_t green[NRGBs] = { 1.00 , 0.60 } ;
	Double_t blue[NRGBs]  = { 1.00 , 0.25 } ;

	TColor::CreateGradientColorTable ( NRGBs , stops , red , green , blue , NCont ) ;
	gStyle -> SetNumberContours ( NCont ) ;
}


void PlottingUtils::set2DCorrelationsColorPalette ()
{
	const Int_t NRGBs = 3 ;
	const Int_t NCont = 255 ;

	Double_t stops[NRGBs] = { 0.00 , 0.50 , 1.00 } ;
	Double_t red[NRGBs]   = { 0.09 , 1.00 , 1.00 } ;
	Double_t green[NRGBs] = { 0.09 , 1.00 , 0.55 } ;
	Double_t blue[NRGBs]  = { 1.00 , 1.00 , 0.55 } ;

	TColor::CreateGradientColorTable ( NRGBs , stops , red , green , blue , NCont ) ;
	gStyle -> SetNumberContours ( NCont ) ;
}


void PlottingUtils::DrawText ( const TString & txt , int col , double y , double x , int align )
{
	static TLatex *tex = new TLatex();
	tex->SetNDC();
	tex->SetTextSize(0.03);
	tex->SetTextAlign(align);
	tex->SetTextColor(col);
	tex->DrawLatex(x,y,txt);
}


void PlottingUtils::DrawText2 ( const TString & txt , int col , double y , double x , double size )
{
	TLatex p ;
	p.SetNDC () ;
	p.SetTextFont ( 42 ) ;
	p.SetTextSize ( size ) ;
	p.SetTextColor ( col ) ;
	p.DrawLatex ( x , y , txt ) ;
}


void PlottingUtils::Label ( const double & x , const double & y , const char* text , const Color_t & color , const float & size , const Font_t & font ) 
{
  	if ( ! text ) return ;
	TLatex p ; 
	p.SetNDC () ;
	p.SetTextFont ( font ) ;
	p.SetTextSize ( size ) ;
	p.SetTextColor ( color ) ;
	p.DrawLatex ( x , y , text ) ;
}


void PlottingUtils::Label ( const double & x , const double & y , const std::string & str_text , const Color_t & color , const float & size , const Font_t & font ) 
{
  	PlottingUtils::Label ( x , y , str_text.c_str() , color , size , font ) ;
}


void PlottingUtils::DrawSubPad ()
{
	double TEXTSIZE = 0.12 , FIGURE2_RATIO = 0.36 , SUBFIGURE_MARGIN = 0.18 ; // white space between figures
	double _maxDev  =0.25 ;
	gPad -> SetBottomMargin ( FIGURE2_RATIO ) ;
	gPad -> SetTopMargin ( 0.05 ) ;
	gPad -> SetFillStyle ( 0 ) ;
	TPad * p = new TPad ( "p_test" , "", 0 , 0.0 , 0.99 , 1.0 - SUBFIGURE_MARGIN , 0 , 0 , 0 ) ;
	p -> SetTopMargin ( 1.0 - FIGURE2_RATIO ) ;
	p -> SetFillStyle ( 0 ) ;
	p -> SetMargin ( 0.16 , 0.04 , 0.18 , 1.0 - FIGURE2_RATIO ) ;
	p -> Draw () ;
	p -> cd () ;
	p -> SetGridy ( kTRUE ) ;
}


void PlottingUtils::ClearSubPad ()
{
	gPad -> Clear () ;
	gPad -> SetBottomMargin ( 0.15 ) ;
	gPad -> SetTopMargin ( 0.05 ) ;
	gPad -> SetFillStyle ( 0 ) ;
}


TGraphAsymmErrors * PlottingUtils::Draw1D ( const VecDouble & content , const VecDouble & errors_low , const VecDouble & errors_up , const VecDouble & bins , const VecDouble & binlims , const int & col , const int & ms , const TString & xtit , const TString & ytit , const bool & orn )
{
	int n = bins.size(); double x[n], y[n], exl[n], eyl[n], exh[n], eyh[n];
	for (size_t i=0, ni=bins.size(); i<ni; ++i) { x[i] = bins[i]; y[i] = content[i]; exl[i] = 0.0; eyl[i] = errors_low[i]; exh[i] = 0.0; eyh[i] = errors_up[i]; };
	TGraphAsymmErrors *h; orn == true ? h = new TGraphAsymmErrors(n,x,y,exl,exh,eyl,eyh) : h = new TGraphAsymmErrors(n,y,x,eyl,eyh,exl,exh);
	h->GetXaxis()->SetTitle(xtit); h->GetYaxis()->SetTitle(ytit);
	h->GetXaxis()->SetLimits(binlims[0]-1.0,binlims[binlims.size()-1]+1.0);
	h->SetLineColor(col); h->SetLineWidth(2);
	h->SetMarkerStyle(ms); h->SetMarkerSize(0.8); h->SetMarkerColor(col); h->SetMinimum(0);
	return h;
}


TH1D * PlottingUtils::Draw1D ( const TString & name , const VecDouble & Values , const VecDouble & Errors , const VecDouble & Bins , const int & col , const int & sty , const int & ms , const TString & xtit , const TString & ytit )
{
	TH1D *hist = new TH1D(name.Data(),"",Bins.size()-1,&Bins[0]);
	hist->SetXTitle(xtit); hist->SetYTitle(ytit);
	hist->SetLineColor(col); hist->SetLineWidth(2); hist->SetLineStyle(sty); hist->SetStats(0);
	hist->SetMarkerStyle(ms); hist->SetMarkerSize(0.8); hist->SetMarkerColor(col); hist->SetMinimum(0);
	for (size_t i=0, ni=Values.size(); i<ni; ++i) {
		hist->SetBinContent(i+1,Values[i]);
        	hist->SetBinError(i+1,Errors[i]);
	}
	return hist;
}


void PlottingUtils::correctToZero ( TGraph * graph )
{
	double minimum ( 0. ) ;
	for ( int idx (0) ; idx < graph->GetN() ; ++ idx )
	{
		double thisY ( * (graph->GetY() + idx) ) ;
		if ( thisY < minimum ) minimum = thisY ;
	}
	for ( int idx (0) ; idx < graph->GetN() ; ++ idx )
	{
		* (graph->GetY() + idx) = * (graph->GetY() + idx) - minimum ;
	}
}


double PlottingUtils::getHistoMax ( TH1 * histo , const bool & includeErrorBars )
{
	double maxY ( - 1e10 ) ;
	for ( int binNum (1) ; binNum <= histo -> GetNbinsX () ; ++ binNum )
	{
		double thisY ( histo -> GetBinContent ( binNum ) ) ;
		if ( includeErrorBars ) thisY += histo -> GetBinError ( binNum ) ;
		if ( thisY < maxY ) continue ;
		maxY = thisY ;
	}
	return maxY ;
}


double PlottingUtils::getHistoMax ( TH2 * histo , const bool & includeErrorBars )
{
	double maxY ( - 1e10 ) ;
	for ( int binNum_x (1) ; binNum_x <= histo -> GetNbinsX () ; ++ binNum_x )
		for ( int binNum_y (1) ; binNum_y <= histo -> GetNbinsY () ; ++ binNum_y )
		{
			double thisY ( histo -> GetBinContent ( binNum_x , binNum_y ) ) ;
			if ( includeErrorBars ) thisY += histo -> GetBinError ( binNum_x , binNum_y ) ;
			if ( thisY < maxY ) continue ;
			maxY = thisY ;
		}
	return maxY ;
}


double PlottingUtils::getHistoMin ( TH1 * histo , const bool & includeErrorBars )
{
	double minY ( 1e10 ) ;
	for ( int binNum (1) ; binNum <= histo -> GetNbinsX () ; ++ binNum )
	{
		double thisY ( histo -> GetBinContent ( binNum ) ) ;
		if ( includeErrorBars ) thisY -= histo -> GetBinError ( binNum ) ;
		if ( thisY > minY ) continue ;
		minY = thisY ;
	}
	return minY ;
}


double PlottingUtils::getHistoMin ( TH2 * histo , const bool & includeErrorBars )
{
	double minY ( 1e10 ) ;
	for ( int binNum_x (1) ; binNum_x <= histo -> GetNbinsX () ; ++ binNum_x )
		for ( int binNum_y (1) ; binNum_y <= histo -> GetNbinsY () ; ++ binNum_y )
		{
			double thisY ( histo -> GetBinContent ( binNum_x , binNum_y ) ) ;
			if ( includeErrorBars ) thisY -= histo -> GetBinError ( binNum_x , binNum_y ) ;
			if ( thisY > minY ) continue ;
			minY = thisY ;
		}
	return minY ;
}


void PlottingUtils::plotFitResults ( PlotFile & plotFile , Fitter & fitter )
{
	PlottingUtils::plotFitResults ( plotFile , fitter.fitVar() , (RooDataSet*) fitter.lastRooDataset() , fitter.lastPdf() , fitter.dataCategory() , fitter.nBins() ) ;
}


void PlottingUtils::plotFitResults ( PlotFile & plotFile , RooRealVar * myy , RooDataSet * dataset , RooAbsPdf * pdf , RooCategory * data_cat , const unsigned int & nBins )
{
	plotFile.cd() ;
	for ( unsigned int binNum ( 1 ) ; binNum <= nBins ; ++binNum )
	{	
    		std::string title ( pdf -> GetTitle() ) ;
		RooPlot* frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;
		dataset -> plotOn ( frame , DataError(RooAbsData::SumW2) , LineColor(12) , Cut(Form("sample == sample::data_bin%d",binNum)) , Binning( myy->getMax()-myy->getMin() , myy->getMin() , myy->getMax() ) ) ;
		if ( title.find ( "bkg" ) != std::string::npos ) pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d", binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure), LineStyle(2), LineWidth(2), ProjWData(RooArgSet(*data_cat), *dataset) ) ;
		if ( title.find ( "sig_bkg" ) != std::string::npos ) pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d", binNum)) , LineColor(kRed) , LineStyle(1) , LineWidth(2), ProjWData(RooArgSet(*data_cat), *dataset) ) ;
		if ( title.find ( "sig" ) != std::string::npos ) pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d", binNum)) , Components(Form("sig_pdf_bin%d",binNum)) , LineColor(kRed), LineStyle(1), LineWidth(2), ProjWData(RooArgSet(*data_cat), *dataset) ) ;
		frame -> GetYaxis() -> SetTitle ( "Events / 1 GeV" ) ;
		frame -> Draw () ;
		plotFile.draw() ;
	}
}


void PlottingUtils::plotSignalFits ( PlotFile & plotFile , Punchcard & punch , Fitter & fitter , const bool & useLogYaxis )
{
	PlottingUtils::plotSignalFits ( plotFile , punch , fitter.fitVar() , (RooDataSet*) fitter.lastRooDataset() , fitter.lastPdf() , fitter.dataCategory() , fitter.nBins() , useLogYaxis ) ;
}

TGraphErrors *PlottingUtils::plotDivision(RooDataSet * dataset, RooAbsPdf * pdf, RooRealVar * myy)
{
  double minOrigin = myy->getMin();
  double maxOrigin = myy->getMax();
  double nEvents = dataset->sumEntries(Form("%s>%f&&%s<%f", myy->GetName(), minOrigin, myy->GetName(), maxOrigin));
  double xBins = maxOrigin - minOrigin;
  TH1F *originHist = (TH1F *)dataset->createHistogram("dataSub", *myy, RooFit::Binning(xBins, minOrigin, maxOrigin));

  TGraphErrors *result = new TGraphErrors();
  double increment = ((maxOrigin - minOrigin) / ((double)xBins));
  //std::cout << "increment = " << increment << std::endl;
  myy->setRange("fullRange", minOrigin, maxOrigin);
  RooAbsReal *intTot = (RooAbsReal *)pdf->createIntegral(RooArgSet(*myy), RooFit::NormSet(*myy),RooFit::Range("fullRange"));
  double valTot = intTot->getVal();
  int pointIndex = 0;
  int pointIndexNonZero = 0;
  //std::cout << "valTot = " << valTot << std::endl;
  for (double i_m = minOrigin; i_m < maxOrigin; i_m += increment) {
    myy->setRange(Form("range%2.2f", i_m), i_m, (i_m + increment));
    RooAbsReal *intCurr
      = (RooAbsReal *)pdf->createIntegral(RooArgSet(*myy), RooFit::NormSet(*myy), RooFit::Range(Form("range%2.2f", i_m)));
    double valCurr = intCurr->getVal();

    //std::cout << "valCurr = " << valCurr << std::endl;

    double currMass = i_m + (0.5 * increment);
    double currPdfWeight = nEvents * (valCurr / valTot);
    TString varName = myy->GetName();
    double currDataWeight
      = dataset->sumEntries(Form("%s>%f&&%s<%f", varName.Data(), i_m, varName.Data(), (i_m + increment)));
    double currWeight = currDataWeight / currPdfWeight;
    //std::cout << "currMass = " << currMass << std::endl;
    //std::cout << "currWeight = " << currWeight << std::endl;
    result->SetPoint(pointIndex, currMass, currWeight);
    //std::cout << "pointIndex = " << pointIndex << ", currWeight = " << currWeight << std::endl;

    double currError = originHist->GetBinError(pointIndex + 1) / currPdfWeight;
    result->SetPointError(pointIndex, 0.0, currError);

    pointIndex++;
  }
  delete originHist;
  return result;
}


void PlottingUtils::plotSignalFits ( PlotFile & plotFile , Punchcard & punch , RooRealVar * myy , RooDataSet * dataset , RooAbsPdf * pdf , RooCategory * data_cat , const unsigned int & nBins , const bool & useLogYaxis )
{
	plotFile.cd() ;
	for ( unsigned int binNum ( 1 ) ; binNum <= nBins ; ++binNum )
	{
		//TPad *pad1 = new TPad("pad1", "pad1", 0.00, 0.33, 1.00, 1.00);
		//TPad *pad2 = new TPad("pad2", "pad2", 0.00, 0.00, 1.00, 0.33);
		//pad1->Draw();
		//pad2->Draw();
		//pad1->cd();
		RooPlot * frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;
		dataset -> plotOn ( frame , LineColor(12) , Cut(Form("sample == sample::data_bin%d",binNum)) , Binning( myy->getMax()-myy->getMin() , myy->getMin() , myy->getMax() ) ) ;
		// dataset -> plotOn ( frame , LineColor(12) , Cut(Form("sample == sample::data_bin%d",binNum)) , Binning(20 ,115 ,135));
		pdf -> plotOn ( frame , LineColor(kRed) , Slice ( *data_cat , Form("data_bin%d", binNum) ) , ProjWData(RooArgSet(*data_cat) , * dataset ) ) ;
		frame -> GetXaxis() -> SetTitle ( "m_{ #gamma#gamma} [GeV]" ) ;
		frame -> GetYaxis() -> SetTitle ( "Events / GeV" ) ;
		//frame -> GetXaxis() -> SetRangeUser(115,135);
		if ( useLogYaxis )
		{
			frame -> SetMaximum ( 13 * frame -> GetMaximum() ) ;
			frame -> SetMinimum ( 1e-6 * frame -> GetMaximum() ) ;
			frame -> SetAxisRange ( 100. , 150. ) ;
			gPad -> SetLogy ( true ) ;
		}
		//frame -> SetMinimum ( 1e-6 * frame -> GetMaximum() ) ;
		frame -> SetMinimum (0.);
		frame -> Draw () ;
		//plotFile.canvas() -> Draw () ;
		PlottingUtils::ATLAS_LABEL ( 0.19 , 0.87 , 1 ) ;
		PlottingUtils::DrawText ( "Internal" , kBlack , 0.882 , 0.323 ) ;
		PlottingUtils::DrawText ( "#it{MC signal shape}" , kBlack , 0.8395 , 0.19 ) ;
		PlottingUtils::DrawText ( punch.getBinLabel(binNum).c_str() , kBlack , 0.7975 , 0.188 ) ;
		//PlottingUtils::DrawText ( "#sqrt{s} = 13.6 TeV" , kBlack , 0.749 , 0.21 ) ;
		PlottingUtils::DrawText2 ( Form("#sqrt{s} = 13.6 TeV, %.1f fb^{-1}",31.4) , kBlack , 0.748, 0.19, 0.03) ;
		PlottingUtils::DrawText ( "#mu = 125.56" , kBlack , 0.87 , 0.65) ;
		PlottingUtils::DrawText ( "#sigma = 1.73" , kBlack , 0.83 , 0.65) ;
		PlottingUtils::DrawText ( "#alpha_{low} = 1.64" , kBlack , 0.79 , 0.65) ;
		PlottingUtils::DrawText ( "#alpha_{high} = 1.53" , kBlack , 0.75 , 0.65) ;
		PlottingUtils::DrawText ( "n_{low} = 5.13" , kBlack , 0.71 , 0.65) ;
		PlottingUtils::DrawText ( "n_{high} = 18.33" , kBlack , 0.67 , 0.65) ;
		//plotFile.draw() ;
		//plotFile.canvas() -> Clear () ;
		gPad -> SetLogy ( false ) ;

		//pad2->cd();
		PlottingUtils::DrawSubPad();

		double rMin = myy->getMin();
		double rMax = myy->getMax();
		int rBins = (int)(rMax - rMin);
		TH1F *medianHist = new TH1F("median", "median", myy->getMax()-myy->getMin(), myy->getMin(), myy->getMax());
		// TH1F *medianHist = new TH1F("median", "median", 20, 115, 135);
		medianHist->SetLineColor(kRed);
                medianHist->SetLineWidth(1);
		medianHist->GetXaxis()->SetTitle("m_{#gamma#gamma}[GeV]");
		for (int i_b = 1; i_b <= rBins; i_b++) { 
		medianHist->SetBinContent(i_b, 1.0);}
		medianHist->GetYaxis()->SetTitle("MC / Fit");
		medianHist->GetYaxis()->SetRangeUser(0., 2.);
		// medianHist->GetYaxis()->SetRangeUser(0.8, 1.2);
		medianHist->GetYaxis()->SetNdivisions(5);
		medianHist->GetXaxis()->SetTitleSize(0.06);
		medianHist->GetYaxis()->SetTitleSize(0.06);
		medianHist->GetXaxis()->SetLabelSize(0.06);
		medianHist->GetYaxis()->SetLabelSize(0.06);
		medianHist->GetXaxis()->SetTitleOffset(0.95);
                medianHist->GetYaxis()->SetTitleOffset(1);
		medianHist->Draw("");

		TGraphErrors *subData = plotDivision(dataset, pdf, myy);
		subData->SetFillColor(4);
		 subData->SetFillStyle(3010);
		//subData->Draw("a3");
                subData->Draw("PE,SAME");
		/*
		TMultiGraph* mg = new TMultiGraph();
		mg->GetYaxis()->SetLimits(0.8, 1.2);
		mg->Add(subData, "P2");
                mg->Draw("nostack,same");
		subData->Draw("same ][");
		*/
		frame -> Print ( "v" ) ;
		plotFile.draw () ;
	}
}


void PlottingUtils::plotFitResiduals ( PlotFile & plotFile , Punchcard & punchcard , Fitter & fitter , const float & lumi , const std::string & ATLASLabel , const bool & plotPulls , const bool & printBkgFunction , const bool & printSigYield )
{
	RooRealVar * myy ( fitter.fitVar() ) ;
	RooDataSet * dataset ( (RooDataSet*) fitter.lastRooDataset() ) ;
	RooAbsPdf * pdf ( fitter.lastPdf() ) ;
	RooCategory * data_cat ( fitter.dataCategory() ) ;
	plotFile.cd() ;
	for ( unsigned int binNum ( 1 ) ; binNum <= fitter.nBins() ; ++binNum )
	{
		RooPlot * frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;
		dataset -> plotOn(frame,DataError(RooAbsData::SumW2),LineColor(12), Cut(Form("sample == sample::data_bin%d",binNum)) , Binning(myy->getMax()-myy->getMin(),myy->getMin(),myy->getMax()) ) ;
		TGraph * graph_data = (TGraph*) frame -> getObject ( frame -> numItems() - 1 ) ;
		pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure) , LineStyle(2) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) ) ;
		TGraph * graph_bkg = (TGraph*) frame -> getObject ( frame -> numItems() - 1  ) ;
		pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , LineColor(kRed) , LineStyle(1) , LineWidth(2) , ProjWData(RooArgSet(*data_cat), * dataset) ) ;
		TGraph * graph_fit = (TGraph*) frame -> getObject ( frame -> numItems() - 1  );
		pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("sig_pdf_bin%d",binNum)) , LineColor(kBlack) , LineStyle(1) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) ) ;
		TGraph * graph_sig = (TGraph*) frame -> getObject ( frame -> numItems() - 1  );
		frame -> GetYaxis() -> SetTitle ( "Events / GeV" ) ;
		frame -> SetMaximum ( 1.2 * frame -> GetMaximum () ) ;
		frame -> Draw () ;
		PlottingUtils::ATLAS_LABEL(0.65,0.87,1);
		//    ATLAS_LABEL(0.70,0.87,1); DrawText2("Internal",kBlack,0.88,0.825,0.03);
		PlottingUtils::DrawText2 ( ATLASLabel.c_str() , kBlack , 0.87 , 0.765 , 0.05 ) ;
		PlottingUtils::DrawText2 ( punchcard.getBinLabel(binNum).c_str() , kBlack , 0.816 , 0.65 , 0.037 ) ;
		//DrawText2 ( Form("%s, H#rightarrow#gamma#gamma", Input::punch()->GetValue("FitVarLabel","Variable")) , kBlack , 0.816 , 0.65 , 0.037 ) ;
		if ( plotPulls )
		{
	        	PlottingUtils::DrawText2("#sqrt{s} = 13 TeV",kBlack,0.77,0.65,0.03);
			PlottingUtils::DrawText2(Form("m_{H} = %.2f GeV",fitter.workspace()->var("mH")->getValV()) , kBlack , 0.73 , 0.65 , 0.03 ) ;
			PlottingUtils::DrawText2(Form("N_{sig} = %.2f #pm %.2f",fitter.workspace()->var(Form("Nsig_bin%d",binNum))->getValV(), fitter.workspace()->var(Form("Nsig_bin%d",binNum))->getError()),kBlack,0.69,0.65,0.03);
			PlottingUtils::DrawText2(Form("Bkg: %s",BackgroundModel::name(punchcard.binBkgFunction(binNum)).c_str()),kBlack,0.65,0.65,0.03);
		}
		else {
	        	PlottingUtils::DrawText2 ( Form("#sqrt{s} = 13 TeV, %.1f fb^{-1}",0.001*lumi) , kBlack , 0.76 , 0.65 , 0.037 ) ;
			if ( printBkgFunction ) PlottingUtils::DrawText2 ( Form("Bkg: %s",BackgroundModel::name(fitter.bkgModelType(binNum)).c_str()) , kBlack , 0.704 , 0.65 , 0.037 ) ;
			if ( printSigYield ) PlottingUtils::DrawText2(Form("N_{sig} = %.2f #pm %.2f",fitter.workspace()->var(Form("Nsig_bin%d",binNum))->getValV(), fitter.workspace()->var(Form("Nsig_bin%d",binNum))->getError()) , kBlack , ( printBkgFunction ? 0.648 : 0.704 ) , 0.65 , 0.037 ) ;
			//DrawText2(Form("H#rightarrow#gamma#gamma, m_{H} = %.2f GeV" , _ws->GetWorkspace()->var("higgs_mass")->getValV()) , kBlack , 0.704 , 0.65 , 0.037 ) ;
		}

		TLegend *leg1 = new TLegend ( 0.3 , 0.73 , 0.61 , 0.91 ) ;
		leg1 -> SetBorderSize ( 0 ) ;
		leg1 -> SetTextSize ( 0.034 ) ;
		leg1 -> SetTextFont ( 42 ) ;
		leg1 -> SetFillColor ( kWhite ) ;
		leg1 -> SetLineColor ( kWhite ) ;
		leg1 -> AddEntry ( graph_data , "Data" , "P" ) ;
		leg1 -> AddEntry ( graph_bkg , "Background" , "L" ) ;
		leg1 -> AddEntry ( graph_fit , "Signal + background" , "L" ) ;
		leg1 -> AddEntry ( graph_sig , "Signal" , "L" ) ;
		leg1 -> Draw () ;

		// Draw a sub pad to add a ratio plot
		PlottingUtils::DrawSubPad();
		RooHist * h_ratio = frame -> getHist ( Form("h_c_data_ds_Cut[sample == sample::data_bin%d]",binNum) ) ;
		RooCurve * c_pdf  = frame -> getCurve ( Form("sig_bkg_pdf_bin%d_Norm[m_yy]",binNum) ) ;
		RooCurve * c_bkg_pdf  = frame -> getCurve ( Form("sig_bkg_pdf_bin%d_Norm[m_yy]_Comp[bkg_pdf_bin%d]",binNum,binNum) ) ;
		//RooCurve* c_sig_pdf  = frame->getCurve(Form("sig_bkg_pdf_bin%d_Norm[m_yy]_Comp[sig_pdf_bin%d]",iBin,iBin));
		TH1 * h_res = NULL ;
		h_res = Utils::residualHist ( h_ratio , ( plotPulls ? c_pdf : c_bkg_pdf ) , plotPulls ) ;
		h_res -> SetLineColor ( 12 ) ;
		h_res -> GetXaxis() -> SetTitle ( "#scale[1.2]{m_{ #gamma#gamma} [GeV]}" ) ;
		float yMax ( 0 ) , yMin ( 0 ) ;
		for ( int binNum ( 1 ) ; binNum <= h_res -> GetNbinsX() ; ++ binNum )
		{
	        	float thisMin ( h_res -> GetBinContent ( binNum ) - h_res -> GetBinError ( binNum ) ) ;
			float thisMax ( h_res -> GetBinContent ( binNum ) + h_res -> GetBinError ( binNum ) ) ;
			if ( thisMin < yMin ) yMin = thisMin ;
			if ( thisMax > yMax ) yMax = thisMax ;
		}
		float range ( yMax - yMin ) ;
		h_res -> GetYaxis() -> SetRangeUser ( yMin - 0.15 * range , yMax + 0.15 * range ) ;
		h_res -> DrawCopy() ;
		TF1 * lineAtZero = new TF1 ( "lineAtZero" , "0" , 100 , 165 ) ;
		lineAtZero -> SetLineWidth ( 2 ) ; lineAtZero -> SetLineStyle ( ( plotPulls ? 1 : 2 ) ) ; lineAtZero -> SetLineColor ( ( plotPulls ? kRed : kAzure ) ) ;
		lineAtZero -> Draw ( "same" ) ;
		RooCurve sig_pdf_black ( * frame -> getCurve(Form("sig_bkg_pdf_bin%d_Norm[m_yy]_Comp[sig_pdf_bin%d]",binNum,binNum)) );
		sig_pdf_black.SetLineColor ( kRed ) ;
		if ( ! plotPulls ) sig_pdf_black.Draw ( "same" ) ;
		//if ( ! plotPulls ) c_sig_pdf -> Draw ( "same" ) ;

		frame -> Print ( "v" ) ;
		plotFile.draw () ;
	}
}


void PlottingUtils::plotNPPulls ( PlotFile & plotFile , Fitter & fitter , Punchcard & punch )
{
	VecStr NPNames ;
	VecDouble NPValues , NPErrorsUp , NPErrorsDown ;
	for ( const auto & NP : fitter.NPs() )
	{
		NPNames.push_back ( NP -> getTitle() . ReplaceAll ( "NPPull_" , "" ) . Data() ) ;
		NPValues.push_back ( NP -> getVal () ) ;
		NPErrorsUp.push_back ( NP -> getErrorHi () ) ;
		NPErrorsDown.push_back ( fabs ( NP -> getErrorLo () ) ) ;
	}
    
	ClearSubPad () ;

	plotFile.canvas () -> SetLeftMargin ( 0.2 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.1 ) ;
    
	TH2D * axis = new TH2D ( "axis" , "axis;Nuisance Parameter Value and Error" , 7 , -1.5 , 1.5 , NPNames.size() + 2 , 0 , NPNames.size() + 2 ) ;
	axis -> SetMinimum ( - 1 ) ;
	axis -> SetMaximum ( NPNames.size() - 1 ) ;
	for ( int i ( NPNames.size() - 1 ) ; i >= 0 ; i-- ) axis -> GetYaxis() -> SetBinLabel ( i+2 , Form("#scale[0.8]{%s}",NPNames[i].c_str()) ) ;
	axis -> GetYaxis() -> SetLabelSize ( 0.025 ) ;
	axis -> GetXaxis() -> SetLabelSize ( 0.04 ) ;
	axis -> GetXaxis() -> SetTitleSize ( 0.04 ) ;
	axis -> GetXaxis() -> SetTitleOffset ( 1.3 ) ;
	axis -> Draw () ;

	VecDouble bins, binlims ( { 1.5 , NPNames.size() + 1.5 } ) ;
	for ( unsigned int i = 0 ; i < NPNames.size() ; i++ ) bins.push_back ( i + 1.5 ) ;
	TGraphAsymmErrors * tgraph_NPs = PlottingUtils::Draw1D(NPValues,NPErrorsDown,NPErrorsUp,bins,binlims,12,1,"Nuisance Parameter Value and Error","Nuisance Parameter",false);
	tgraph_NPs -> SetMarkerStyle ( 25 ) ;
	tgraph_NPs -> SetMarkerColor ( 56 ) ;
	tgraph_NPs -> SetLineColor ( 12 ) ;
	tgraph_NPs -> Draw ( "P" ) ;
	PlottingUtils::ATLAS_LABEL ( 0.72 , 0.923 , 1 ) ;
	PlottingUtils::Label ( 0.84 , 0.922 ,"Internal" , kBlack , 0.038 ) ; 
	PlottingUtils::Label ( 0.21 , 0.925 , Form ( "Distribution: %s" , punch.tag().c_str() ) , kBlack , 0.04 ) ; 
	TArrow sig_min(-1,1,-1,NPNames.size()+1.,0.05,"");sig_min.SetLineColor(62); sig_min.SetLineStyle(7); sig_min.Draw("same");
	TArrow sig_max(1,1,1,NPNames.size()+1.,0.05,""); sig_max.SetLineColor(62); sig_max.SetLineStyle(7); sig_max.Draw("same");

	plotFile.draw () ;
	plotFile.canvas () -> Clear () ;
}


void PlottingUtils::plotDataMCRatio ( PlotFile & plotFile , TH1D * h_data , TH1D * h_mc )
{
	TH1D * h_ratio_mc = new TH1D ( * h_mc ) ;
	h_ratio_mc -> Divide ( h_mc ) ;
	h_ratio_mc -> Sumw2 ( false ) ;
	h_ratio_mc -> GetYaxis () -> SetTitle ( "Fit yield / MC" ) ;

	TH1D * h_ratio_data = new TH1D ( * h_data ) ;
	if ( h_data -> GetNbinsX () != h_mc -> GetNbinsX () ) Utils::Fatal ( "PlottingUtils::plotDataMCRatio(...)" , "Data and MC have different numbers of bins" ) ;
	for ( int binNum (1) ; binNum <= h_data -> GetNbinsX () ; ++ binNum )
	{
    		h_ratio_data -> SetBinContent ( binNum , h_data -> GetBinContent ( binNum ) / h_mc -> GetBinContent ( binNum ) ) ;
		h_ratio_data -> SetBinError   ( binNum , h_data -> GetBinError ( binNum )   / h_mc -> GetBinContent ( binNum ) ) ;
	}

	float min ( getHistoMin ( h_ratio_data , true ) ) ;
	float max ( getHistoMax ( h_ratio_data , true ) ) ;
	h_ratio_mc -> GetYaxis () -> SetRangeUser ( min - 0.1 * ( max - min ) , max > 1.05 ? max + 0.1 * ( max - min ) : 1.05 ) ;

	h_ratio_mc -> Draw () ;
	h_ratio_data -> Draw ( "same" ) ;

	plotFile . draw () ;

	Utils::deleteAndNullify ( h_ratio_data ) ;
	Utils::deleteAndNullify ( h_ratio_mc   ) ;
}


void PlottingUtils::plotExtractedYields ( PlotFile & plotFile , Fitter & fitter , Punchcard & punchcard , UnbinnedInput & dataset )
{
	if ( punchcard.nVars() == 2 )
	{
		plotExtractedYields2D ( plotFile , fitter , punchcard , dataset ) ;
		return ;
	}
	if ( punchcard.nVars() > 2 )
	{
		Utils::Error ( "PlottingUtils::plotExtractedYields(...)" , "Cannot plot yields for tag " + punchcard.tag() + " because it is not implemented for nVars > 2" ) ;
		return ;
	}
	VecDouble Yields, Errors;
	double ymin = 0., ymax = 0.;
	for ( unsigned int binNum ( 1 ) ; binNum <= punchcard.nBins_total() ; ++ binNum )
	{
		Yields.push_back ( fitter.workspace() -> var(Form("Nsig_bin%d",binNum)) -> getValV() ) ;
		Errors.push_back ( fitter.workspace() -> var(Form("Nsig_bin%d",binNum)) -> getError() ) ;
	}
    
	VecDouble AllBins ;
	for ( const std::string & str_bin : punchcard.binBoundaries() ) AllBins.push_back ( std::stod ( str_bin ) ) ;
	bool firstBinIsUnderflow ( punchcard.firstBinIsUnderflow() ) ;
	bool lastBinIsOverflow ( punchcard.lastBinIsOverflow() ) ;
	bool isFiducialPlot ( firstBinIsUnderflow && lastBinIsOverflow && ( AllBins.size() == 3 || AllBins.size() == 2 ) ) ;
	if ( isFiducialPlot )
	{
		if ( AllBins.size () == 3 ) AllBins = VecDouble ( { 0. , 1. , 2. } ) ;
		if ( AllBins.size () == 2 ) AllBins = VecDouble ( { 0. , 1. } ) ;
		firstBinIsUnderflow = false ; lastBinIsOverflow = false ;
	}
	VecDouble HistoBins ( AllBins ) ;
	if ( HistoBins.back() == 999 ) HistoBins.back() = HistoBins[HistoBins.size()-2] + 1 ;

	std::pair < float , float > underflow , overflow , underflowMC , overflowMC ;
	if ( firstBinIsUnderflow ) HistoBins . erase ( HistoBins.begin () ) ;
	if ( lastBinIsOverflow ) HistoBins . erase ( HistoBins.end () - 1 ) ;

	float pad_max ( 1.0 ) ;
	if ( firstBinIsUnderflow ^ lastBinIsOverflow ) pad_max = 0.8 ;
	if ( firstBinIsUnderflow && lastBinIsOverflow ) pad_max = 0.74 ;
	TPad * pad = new TPad ( "pad" , "" , 0 , 0.0 , pad_max , 1.0 , 0 , 0 , 0 ) ;
	TPad * pad2 = new TPad ( "pad2" , "" , pad_max , 0.0 , 1.0 , 1.0 , 0 , 0 , 0 ) ;
	pad -> Draw () ;
	pad2 -> Draw () ;
	pad -> cd () ;
        
	TH1D * fit_allBins = PlottingUtils::Draw1D("fit_allBins",Yields,Errors,AllBins);
	TH1D * fit_histoBins = PlottingUtils::Draw1D("fit_histoBins",Yields,Errors,HistoBins);
	double y_min = 10000;
    
	int binNum ( 1 ) ;
	for ( unsigned int i = 0 ; i < AllBins.size () ; i++ )
	{
        	if ( firstBinIsUnderflow && i == 0 )
		{
			underflow.first = fit_allBins -> GetBinContent ( i + 1 ) ;
			underflow.second = fit_allBins -> GetBinError ( i + 1 ) ;
			continue ;
		}
		if ( lastBinIsOverflow && i == AllBins.size () - 2 )
		{
			overflow.first = fit_allBins -> GetBinContent ( i + 1 ) ;
			overflow.second = fit_allBins -> GetBinError ( i + 1 ) ;
			continue ;
		}
		fit_histoBins -> SetBinContent ( binNum , fit_allBins->GetBinContent( i + 1 ) / fit_histoBins->GetXaxis()->GetBinWidth( binNum ) ) ;
		fit_histoBins -> SetBinError ( binNum , fit_allBins->GetBinError( i + 1 ) / fit_histoBins->GetXaxis()->GetBinWidth( binNum ) ) ;
		float y_min_this ( fit_histoBins->GetBinContent( binNum ) - 1.2 * fabs ( fit_histoBins->GetBinError( binNum ) ) ) ;
		y_min = ( ymin > y_min_this ? y_min_this : y_min ) ;
		++ binNum ;
	}
    
	if(y_min == 10000) y_min = 0;
    
	fit_histoBins->SetMarkerStyle(9);

	fit_histoBins->SetMaximum(2*fit_histoBins->GetMaximum());
	fit_histoBins->SetMinimum(y_min);

	VecStr BinLabels = punchcard.binLabels();    
	if(BinLabels.size() != 0 && (int) BinLabels.size() == fit_histoBins->GetXaxis()->GetNbins())
		for(unsigned int i = 0; i < BinLabels.size(); i++)
     			fit_histoBins->GetXaxis()->SetBinLabel(i+1,BinLabels[i].c_str());
    
	TString FitVarLabel = punchcard.varLabel();
	TString FitVarUnitLabel = punchcard.varUnit();
	FitVarUnitLabel = " " + FitVarUnitLabel;
	fit_histoBins->GetXaxis()->SetTitle(FitVarLabel+FitVarUnitLabel);
	fit_histoBins->GetYaxis()->SetTitle("#it{Fit Yield / Bin width}");
        
	// Now plot MC expectation for reference mass
	auto *h_mc = (TH1D*) fit_histoBins->Clone("mc_pred");
    
	binNum = 1 ;
	for ( unsigned int i ( 0 ) ; i < punchcard.nBins_total() ; i++ )
	{
    		dataset.getBinnedMC(false) -> at( i ) -> Draw ( "m_yy >> h_mcYield(55,105,160)" , dataset.weightVar().c_str() , "" ) ;
		auto *h_mcYield = (TH1D*) gROOT->Get("h_mcYield");
		if ( firstBinIsUnderflow && i == 0 )
        	{
	       		underflowMC.first = h_mcYield -> Integral () ;
			continue ;
		}
		if ( lastBinIsOverflow && i == AllBins.size () - 2 )
        	{
			overflowMC.first = h_mcYield -> Integral () ;
			continue ;
		}
		h_mc -> SetBinContent ( binNum , h_mcYield -> Integral () / h_mc->GetXaxis()->GetBinWidth(binNum) ) ;
		h_mc -> SetBinError ( binNum , 0. ) ;
		++binNum ;
	}
    
	h_mc->SetLineStyle(kGray);
	h_mc->SetFillColor(kGray);
	h_mc->SetFillStyle(3001);
        
	h_mc->SetMaximum(fit_histoBins->GetMaximum()); h_mc->SetMinimum(y_min);
    
	h_mc->Draw("Fhist"); 
	fit_histoBins->Draw("same");

	y_min = 10000 ;
	double y_max = 0 ;
	if ( firstBinIsUnderflow || lastBinIsOverflow )
	{
		pad2 -> cd () ;
		pad2 -> SetLeftMargin ( 0.5 ) ;
		VecDouble Yields2, Errors2, ExtraBins { 0 } ;
		if ( firstBinIsUnderflow )
		{
			Yields2.push_back ( underflow.first ) ;
			Errors2.push_back ( underflow.second ) ;
			ExtraBins.push_back ( 1. ) ;
		}
		if ( lastBinIsOverflow )
		{
			Yields2.push_back ( overflow.first ) ;
			Errors2.push_back ( overflow.second ) ;
			ExtraBins.push_back ( 2. ) ;
		}
		TH1* fit_extraBins = Draw1D("fit_extraBins",Yields2,Errors2,ExtraBins);
		fit_extraBins->SetMarkerStyle(9);
		auto * h_mc_extraBins = (TH1D*) fit_extraBins->Clone("mc_pred_extraBins");
		if ( firstBinIsUnderflow )
		{
			h_mc_extraBins -> SetBinContent ( 1 , underflowMC.first ) ;
			h_mc_extraBins -> GetXaxis () -> SetBinLabel ( 1 , "Underflow" ) ;
			y_min = ( y_min > underflowMC.first ? underflowMC.first : y_min ) ;
			y_max = ( y_max < underflowMC.first ? underflowMC.first : y_max ) ;
			y_min = ( y_min > underflow.first - fabs(underflow.second) ? underflow.first - fabs(underflow.second) : y_min ) ;
			y_max = ( y_max < underflow.first + fabs(underflow.second) ? underflow.first + fabs(underflow.second) : y_max ) ;
		}
		if ( lastBinIsOverflow )
		{
			h_mc_extraBins -> SetBinContent ( h_mc_extraBins -> GetNbinsX() , overflowMC.first ) ;
			h_mc_extraBins -> GetXaxis () -> SetBinLabel ( h_mc_extraBins -> GetNbinsX() , "Overflow" ) ;
			y_min = ( y_min > overflowMC.first ? overflowMC.first : y_min ) ;
			y_max = ( y_max < overflowMC.first ? overflowMC.first : y_max ) ;
			y_min = ( y_min > overflow.first - fabs(overflow.second) ? overflow.first - fabs(overflow.second) : y_min ) ;
			y_max = ( y_max < overflow.first + fabs(overflow.second) ? overflow.first + fabs(overflow.second) : y_max ) ;
		}
		h_mc_extraBins -> GetXaxis () -> SetLabelSize ( 0.16 ) ;
		h_mc_extraBins -> GetYaxis () -> SetLabelSize ( 0.14 ) ;
		h_mc_extraBins -> GetYaxis () -> SetTitleSize ( 0.15 ) ;
		h_mc_extraBins -> GetXaxis () -> LabelsOption ( "u" ) ;
		h_mc_extraBins -> GetYaxis() -> SetTitle("#it{Fit Yield}");    
		h_mc_extraBins->SetLineStyle(kGray);
		h_mc_extraBins->SetFillColor(kGray);
		h_mc_extraBins->SetFillStyle(3001); 
		h_mc_extraBins->GetYaxis()->SetRangeUser(y_min-0.2*fabs(y_min),y_max+0.2*fabs(y_max));
		h_mc_extraBins -> Draw ( "Fhist" ) ;
		fit_extraBins -> Draw ( "same" ) ;
	}
	
	plotFile.draw();

	plotDataMCRatio ( plotFile , fit_histoBins , h_mc ) ;

	delete fit_allBins ;
	delete fit_histoBins ;
}


void PlottingUtils::plotExtractedYields2D ( PlotFile & plotFile , Fitter & fitter , Punchcard & punchcard , UnbinnedInput & dataset )
{
	if ( punchcard.nVars() != 2 )
	{
		Utils::Error ( "PlottingUtils::plotExtractedYields2D(...)" , "Cannot plot yields for variable " + punchcard.tag() + " because nVars != 2" ) ;
		return ;
	}

	set2DSignalColorPalette () ;

	plotFile.canvas () -> SetBottomMargin ( 0.10 ) ;
	plotFile.canvas () -> SetLeftMargin ( 0.2 ) ;
	plotFile.canvas () -> SetRightMargin ( 0.02 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.10 ) ;

	unsigned int nBins_x ( punchcard.nBins ( 1 ) ) ;
	unsigned int nBins_y ( punchcard.nBins ( 2 ) ) ;

	TH2F * histo_2D_yields = new TH2F ( "histo_2D_yields" , "" , nBins_x , 0 , nBins_x , nBins_y , 0 , nBins_y ) ;

	for ( unsigned int binNum_x (1) ; binNum_x <= nBins_x ; ++ binNum_x ) histo_2D_yields -> GetXaxis() -> SetBinLabel ( binNum_x , punchcard.getBinLabel ( 1 , binNum_x ) . c_str() ) ;
	for ( unsigned int binNum_y (1) ; binNum_y <= nBins_y ; ++ binNum_y ) histo_2D_yields -> GetYaxis() -> SetBinLabel ( binNum_y , punchcard.getBinLabel ( 2 , binNum_y ) . c_str() ) ;

	for ( unsigned int binNum ( 1 ) ; binNum <= fitter.nBins() ; ++ binNum )
	{
		VecUInt binIndices ( punchcard.getBinIndices ( binNum ) ) ;
		histo_2D_yields -> SetBinContent ( binIndices.at(0) , binIndices.at(1) , fitter.workspace() -> var ( Form("Nsig_bin%d",binNum) ) -> getVal() ) ;
		histo_2D_yields -> SetBinError ( binIndices.at(0) , binIndices.at(1) , fitter.workspace() -> var ( Form("Nsig_bin%d",binNum) ) -> getError() ) ;
	}

	gStyle -> SetPaintTextFormat ( "2.2f%" ) ;
	histo_2D_yields -> GetXaxis () -> SetTitleOffset ( 1.8 ) ;
	histo_2D_yields -> GetYaxis () -> SetTitleOffset ( 1.6 ) ;
	histo_2D_yields -> GetZaxis () -> SetRangeUser ( 0.0 , getHistoMax ( histo_2D_yields , false ) ) ;
	histo_2D_yields -> SetMarkerSize ( 1.5 ) ;
	histo_2D_yields -> GetXaxis () -> SetLabelSize ( 0.035 ) ;
	histo_2D_yields -> GetYaxis () -> SetLabelSize ( 0.035 ) ;
	histo_2D_yields -> GetXaxis () -> LabelsOption ( "h" ) ;

	histo_2D_yields -> Draw ( "colortextE" ) ;
	ATLAS_LABEL ( 0.72 , 0.91 , 1 ) ;
	Label ( 0.84 , 0.91 , "Internal" , kBlack , 0.04 ) ; 

	plotFile.draw() ;
	plotFile.canvas() -> Clear () ;

	delete histo_2D_yields ;
}


void PlottingUtils::plotLikelihoodScans ( PlotFile & plotFile , Punchcard & punch , const float & lumi , const std::string & ATLASLabel , const unsigned int & binNum , TGraph * scan_fixedNPs , TGraph * scan_floatingNPs )
{
	const double xMin ( * scan_floatingNPs -> GetX() ) ;
	const double xMax ( * ( scan_floatingNPs -> GetX() + scan_floatingNPs -> GetN() - 1 ) ) ;
	
	// make curves
	scan_floatingNPs -> SetName ( "scan_floatingNPs" ) ;
	scan_floatingNPs -> SetTitle ( "Stat + syst" ) ;
	scan_floatingNPs -> SetLineColor ( kRed + 1 ) ;

	scan_fixedNPs -> SetName ( "scan_fixedNPs" ) ;
	scan_fixedNPs -> SetTitle ( "Stat only" ) ;
	scan_fixedNPs -> SetLineColor ( kRed + 1 ) ;
	scan_fixedNPs -> SetLineStyle ( 2 ) ;

	// make plot
	plotFile.canvas () -> SetRightMargin ( 0.05 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.05 ) ;
	plotFile.canvas () -> SetLeftMargin ( 0.15 ) ;
	plotFile.canvas () -> SetBottomMargin ( 0.15 ) ;
	TH1F * frame ( plotFile.canvas () -> DrawFrame ( 1.1 * xMin - 0.1 * xMax , 0 , 1.55 * xMax - 0.55 * xMin , 1.05 ) ) ;
	frame -> SetTitle ( "" ) ;
	frame -> SetLineColor ( kRed + 1 ) ;
	frame -> GetXaxis() -> SetLabelSize ( 0.045 ) ;
	frame -> GetXaxis() -> SetTitleSize ( 0.045 ) ;
	frame -> GetXaxis() -> SetTitleOffset ( 1.5 ) ;
	frame -> GetXaxis() -> SetTitle ( "N_{sig}" ) ;
	frame -> GetYaxis() -> SetLabelSize ( 0.045 ) ;
	frame -> GetYaxis() -> SetTitleSize ( 0.045 ) ;
	frame -> GetYaxis() -> SetTitleOffset ( 1.5 ) ;
	frame -> GetYaxis() -> SetTitle ( "- 2 ln #Lambda" ) ;
	frame -> Draw () ;
	scan_floatingNPs -> Draw ( "sameC" ) ;
	scan_fixedNPs -> Draw ( "sameC" ) ;

	TLegend * leg = new TLegend ( 0.7 , 0.52 , 0.85 , 0.68 ) ;
	leg -> SetLineWidth ( 0 ) ;
	leg -> SetBorderSize ( 0 ) ;
	leg -> SetTextFont ( 132 ) ;
	leg -> AddEntry ( scan_floatingNPs , "Stat + syst" , "l" ) ;
	leg -> AddEntry ( scan_fixedNPs , "Stat only" , "l" ) ;
	leg -> Draw () ;

	TLine * lineAtOne = new TLine ( 1.1 * xMin - 0.1 * xMax , 1 , 1.55 * xMax - 0.55 * xMin , 1 ) ;
	lineAtOne -> SetLineStyle ( 2 ) ;
	lineAtOne -> Draw ( "same" ) ;

	ATLAS_LABEL ( 0.7 , 0.85 , 1 ) ;
	Label ( 0.82 , 0.8505 , ATLASLabel , kBlack , 0.04 ) ;
	Label ( 0.7 , 0.78 , punch.getBinLabel ( binNum ) , kBlack , 0.035 ) ;
	Label ( 0.7 , 0.71 , Form("%.1f fb^{-1}, #sqrt{s} = 13 TeV",0.001*lumi) , kBlack , 0.035 ) ;

	plotFile.draw() ;
	plotFile.canvas() -> Clear() ;

	Utils::deleteAndNullify ( leg ) ;
	Utils::deleteAndNullify ( lineAtOne ) ;
}


void PlottingUtils::writeFitResultToFile ( RooFitResult * result , Fitter & fitter , Punchcard & punchcard , const std::string & filename )
{
	std::ofstream outFile ( filename ) ;
	if ( ! outFile.is_open() ) Utils::Fatal ( "PlottingUtils::writeFitResultToFile()" , "File " + filename + " could not be opened" ) ;

	double sumYields ( 0 ) ;
	double sumYieldsUncertaintySquared ( 0 ) ;
	double sumYieldsUncertaintySquaredMinos ( 0 ) ;

	for ( unsigned int binNum ( 1 ) ; binNum <= punchcard.nBins_total() ; ++ binNum )
	{
    		const char * thisBinSuffix (  Form ( "bin%d" , binNum ) ) ;
		outFile << "\n##  __________________________________________________________________" << std::endl ;
		outFile << "##  ___  Bin " << binNum << std::endl << std::endl ;

		outFile << Form("Nsig_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) -> getValV() << std::endl ;
		outFile << Form("NsigErr_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) -> getError() << std::endl ;
		outFile << Form("NsigErrHi_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) -> getErrorHi() << std::endl ;
		outFile << Form("NsigErrLo_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) -> getErrorLo() << std::endl ;
		outFile << Form("Nbkg_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nbkg_%s",thisBinSuffix) ) -> getValV() << std::endl ;
		outFile << Form("NbkgErr_%s",thisBinSuffix) << ": " << fitter.workspace() -> var ( Form("Nbkg_%s",thisBinSuffix) ) -> getError() << std::endl ;

		outFile << Form("SignalModel_%s: ",thisBinSuffix) <<  SignalModel::name ( fitter.sigModelType() ) << std::endl ;
		for ( auto & param : fitter.sigParams() )
		{
			std::string paramName ( param -> getTitle() ) ;
			if ( paramName.find ( thisBinSuffix ) == std::string::npos ) continue ;
			if ( paramName.find ( thisBinSuffix ) != paramName.size() - std::string(thisBinSuffix).size() ) continue ;
			outFile << param -> getTitle() << ": " << param -> getValV() << std::endl ;
		}
		outFile << Form("BkgModel_%s: ",thisBinSuffix) <<  BackgroundModel::name ( fitter.bkgModelType ( binNum ) ) << std::endl ;
		for ( auto & param : fitter.bkgParams() )
		{
			std::string paramName ( param -> getTitle() ) ;
			if ( paramName.find ( thisBinSuffix ) == std::string::npos ) continue ;
			if ( paramName.find ( thisBinSuffix ) != paramName.size() - std::string(thisBinSuffix).size() ) continue ;
			outFile << param -> getTitle() << ": " << param -> getValV() << std::endl ;
			outFile << param -> getTitle() << "_err: " << param -> getError() << std::endl ;
		}

		sumYields += fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) -> getValV() ;
		for ( unsigned int binNum2 ( 1 ) ; binNum2 <= punchcard.nBins_total() ; ++ binNum2 )
		{
			RooRealVar* yield1 = fitter.workspace() -> var ( Form("Nsig_%s",thisBinSuffix) ) ;
			RooRealVar* yield2 = fitter.workspace() -> var ( Form("Nsig_bin%d",binNum2) ) ;

			double corr = result -> correlation ( Form("Nsig_%s",thisBinSuffix) , Form("Nsig_bin%d",binNum2) ) ;
			double err_bin1 = yield1 -> getError() ;
			double err_bin2 = yield2 -> getError() ;
			sumYieldsUncertaintySquared += corr * err_bin1 * err_bin2;
			double err_bin1_minos = ( yield1 -> getErrorHi() - yield1 -> getErrorLo() ) / 2.0;
			double err_bin2_minos = ( yield2 -> getErrorHi() - yield2 -> getErrorLo() ) / 2.0;
			sumYieldsUncertaintySquaredMinos += corr * err_bin1_minos * err_bin2_minos;
			if ( binNum <= binNum2 ) continue ;
			outFile << "Correlation." << Form("Nsig_%s",thisBinSuffix) << "." << Form("Nsig_bin%d",binNum2)
				<< ": " << corr << std::endl ;
		}
	}

	outFile << "\n##  __________________________________________________________________" << std::endl ;
	outFile << "##  ___  Nuisance parameters " << std::endl << std::endl ;

	for ( auto & param : fitter.NPs() )
	{
		outFile << param -> getTitle() << ": " << param -> getValV() << std::endl ;
		outFile << param -> getTitle() << "_errUp: " << param -> getErrorHi() << std::endl ;
		outFile << param -> getTitle() << "_errDown: " << param -> getErrorLo() << std::endl ;
	}

	outFile << "\n##  __________________________________________________________________" << std::endl ;
	outFile << "##  ___  Total yields " << std::endl << std::endl ;

	outFile << "Nsig_total: " << sumYields << std::endl ;
	outFile << "NsigErr_total: " << sqrt(sumYieldsUncertaintySquared) << std::endl ;
	outFile << "NsigErrMinos_total: " << sqrt(sumYieldsUncertaintySquaredMinos) << std::endl ;

	outFile << "\n##  __________________________________________________________________" << std::endl ;
	outFile << "##  ___  Extras " << std::endl << std::endl ;

	/*for ( unsigned int binNum ( 1 ) ; binNum <= fitter.nBins() ; ++ binNum )
	{
	    float chi2 ( getChiSquared ( fitter.dataCategory() , fitter.lastRooDataset() , fitter.lastPdf() , fitter.fitVar() , binNum , false , nDoF[iBin] ) ) ;
	    float sideband_chi2 ( getChiSquared ( fitter.dataCategory() , fitter.lastRooDataset() , fitter.lastPdf() , fitter.fitVar() , binNum , true , nDoF[iBin] ) ) ;
	    outFile << Form("Fit.chi2.bin%d: ",binNum) << chi2 << std::endl ;
	    outFile << Form("Fit.sideband.chi2.bin%d: ",binNum2) << sideband_chi2 << std::endl ;
	}*/
	outFile << "FitRange: " << FitRange::name ( fitter.fitRangeType() ) << std::endl ;

	outFile.close () ;
}


void PlottingUtils::writeSignalParametersToFile ( Fitter & fitter , Punchcard & punchcard , const std::string & filename )
{
	std::ofstream outFile ( filename ) ;
	if ( ! outFile.is_open() ) Utils::Fatal ( "PlottingUtils::writeSignalParametersToFile()" , "File " + filename + " could not be opened" ) ;

	std::time_t current_time ( std::chrono::system_clock::to_time_t ( std::chrono::system_clock::now() ) ) ;
	std::string time_message ( "##  Automatic output file created by HGamFitter::PlottingUtils::writeSignalParametersToFile (...) on " + std::string ( std::ctime ( & current_time ) ) ) ;
	for ( unsigned int i (0) ; i < time_message.size() ; ++ i ) outFile << "#" ;
	outFile << std::endl ;
	outFile << time_message ;
	for ( unsigned int i (0) ; i < time_message.size() ; ++ i ) outFile << "#" ;
	outFile << std::endl << std::endl ;

	for ( unsigned int binNumber (1) ; binNumber <= fitter.nBins() ; ++ binNumber ) outFile << "Bin" << binNumber << ": " << punchcard.getBinLabel ( binNumber ) << std::endl ;
	outFile << std::endl ;

	std::string vars ( "Vars:" ) ;
	for ( RooRealVar * & var : fitter.sigParams() )
	{
		vars += " " + std::string ( var -> GetName() ) ;
		outFile << var -> GetName() << ": " << var -> getMin() << " " << var -> getMax() << " " << var -> getValV() << " " << var -> getError() << std::endl ;
	}
	outFile << std::endl << vars << std::endl << std::endl ;
	for ( unsigned int i (0) ; i < time_message.size() ; ++ i ) outFile << "#" ;
	outFile << std::endl ;
}


void PlottingUtils::plotPull ( PlotFile & plotFile , CalculateCorrelationsConfig & config , CorrelationsFile & correlationsFile , const std::string & var , const unsigned int & binNum , const std::string & ATLASLabel , const float & lumi )
{
	plotFile.canvas () -> SetBottomMargin ( 0.18 ) ;
	plotFile.canvas () -> SetLeftMargin ( 0.2 ) ;
	plotFile.canvas () -> SetRightMargin ( 0.02 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.02 ) ;

	TH1F * histo ( correlationsFile.pull ( var , binNum ) ) ;

	histo -> GetXaxis () -> SetTitleOffset ( 1.6 ) ;
	histo -> GetYaxis () -> SetTitleOffset ( 1.3 ) ;
	histo -> GetXaxis () -> SetLabelSize ( 0.04 ) ;
	histo -> GetYaxis () -> SetLabelSize ( 0.04 ) ;
	histo -> GetXaxis () -> SetTitleSize ( 0.04 ) ;
	histo -> GetYaxis () -> SetTitleSize ( 0.05 ) ;

	histo -> GetXaxis() -> SetTitle ( "#frac{#nu^{toy} - #nu^{data}}{#Delta #nu^{toy}}" ) ;
	histo -> GetYaxis() -> SetTitle ( "Number of pseudo-experiments" ) ;
	histo -> GetYaxis() -> SetRangeUser ( 0. , 1.15 * PlottingUtils::getHistoMax ( histo , false ) ) ;

	histo -> SetFillColorAlpha ( kAzure - 2 , 0.2 ) ;
	histo -> Draw () ;

	//PlottingUtils::Label ( 0.24 , 0.92 , "File tag: " + config.tag() , kBlack , 0.035 ) ;

	PlottingUtils::ATLAS_LABEL ( 0.24 , 0.91 , 1 ) ;
	PlottingUtils::DrawText2 ( ATLASLabel.c_str() , kBlack , 0.91 , 0.36 , 0.04 ) ;
	PlottingUtils::DrawText2 ( Form("#sqrt{s} = 13 TeV, %.1f fb^{-1}",0.001*lumi) , kBlack , 0.86 , 0.24 , 0.035 ) ;

	PlottingUtils::Label ( 0.24 , 0.81 , correlationsFile.binLabel ( var , binNum ) , kBlack , 0.035 ) ;
	PlottingUtils::Label ( 0.24 , 0.76 , Form ( "N_{toys}: %d" , (int) histo -> GetEntries() ) , kBlack , 0.035 ) ;
	if ( config.strategy() == UnbinnedSignalExtraction ) PlottingUtils::Label ( 0.24 , 0.71 , "Unbinned signal extraction" , kBlack , 0.035 ) ;
	else if ( config.strategy() == BinnedSignalExtraction ) PlottingUtils::Label ( 0.24 , 0.71 ,  "Binned signal extraction" , kBlack , 0.035 ) ;
	else if ( config.strategy() == CountSidebandsUsingToys ) PlottingUtils::Label ( 0.24 , 0.71 ,  "Sideband counting (using toys)" , kBlack , 0.035 ) ;
	else PlottingUtils::Label ( 0.24 , 0.71 , Form ( "Strategy unknown" ) , kBlack , 0.035 ) ;
	PlottingUtils::Label ( 0.24 , 0.66 , ( config.useToyFixedNPs() ? "Fixed signal pulls" : "Floating signal pulls" ) , kBlack , 0.035 ) ;

	PlottingUtils::Label ( 0.24 , 0.61 , Form ( "Mean: %0.3f #pm %0.3f" , histo -> GetMean () , histo -> GetMeanError () ) , kRed + 2 , 0.035 ) ;
	PlottingUtils::Label ( 0.24 , 0.56 , Form ( "RMS: %0.3f #pm %0.3f" , histo -> GetRMS () , histo -> GetRMSError () ) , kRed + 2 , 0.035 ) ;

	plotFile.draw() ;
	plotFile.canvas() -> Clear () ;
}


void PlottingUtils::plotScatter ( PlotFile & plotFile , CalculateCorrelationsConfig & config , CorrelationsFile & correlationsFile , const std::string & var1 , const unsigned int & binNum1 , const std::string & var2 , const unsigned int & binNum2 )
{
	plotFile.canvas () -> SetBottomMargin ( 0.18 ) ;
	plotFile.canvas () -> SetLeftMargin ( 0.2 ) ;
	plotFile.canvas () -> SetRightMargin ( 0.07 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.05 ) ;

	TGraph * graph ( correlationsFile.scatter ( var1 , binNum1 , var2 , binNum2 ) ) ;

	graph -> Draw ( "APX" ) ;

	graph -> GetXaxis () -> SetTitleOffset ( 1.6 ) ;
	graph -> GetYaxis () -> SetTitleOffset ( 1.8 ) ;
	graph -> GetXaxis () -> SetLabelSize ( 0.04 ) ;
	graph -> GetYaxis () -> SetLabelSize ( 0.04 ) ;
	graph -> GetXaxis () -> SetTitleSize ( 0.04 ) ;
	graph -> GetYaxis () -> SetTitleSize ( 0.04 ) ;
	graph -> GetXaxis () -> SetTitle ( Form ( "N_{evt}^{toy} ( %s )" , correlationsFile.binLabel( var1 , binNum1 , var2 , binNum2 ).first.c_str() ) ) ;
	graph -> GetYaxis () -> SetTitle ( Form ( "N_{evt}^{toy} ( %s )" , correlationsFile.binLabel( var1 , binNum1 , var2 , binNum2 ).second.c_str() ) ) ;

	graph -> SetMarkerColor ( kRed + 2 ) ;

	PlottingUtils::Label ( 0.24 , 0.89 , "File tag: " + config.tag() , kBlack , 0.035 ) ;
	PlottingUtils::Label ( 0.24 , 0.84 , Form ( "N_{toys}: %d" , graph -> GetN() ) , kBlack , 0.035 ) ;
	std::string str_strategyNPs ( config.useToyFixedNPs() ? "(fixed NPs)" : "(float NPs)" ) ;
	if ( config.strategy() == UnbinnedSignalExtraction ) PlottingUtils::Label ( 0.24 , 0.79 , "Unbinned signal extraction " + str_strategyNPs , kBlack , 0.035 ) ;
	else if ( config.strategy() == BinnedSignalExtraction ) PlottingUtils::Label ( 0.24 , 0.79 ,  "Binned signal extraction " + str_strategyNPs , kBlack , 0.035 ) ;
	else if ( config.strategy() == CountSidebandsUsingToys ) PlottingUtils::Label ( 0.24 , 0.79 ,  "Sideband counting (using toys)" , kBlack , 0.035 ) ;
	else PlottingUtils::Label ( 0.24 , 0.79 , "Strategy unknown" , kBlack , 0.035 ) ;

	PlottingUtils::Label ( 0.24 , 0.74 , Form ( "#rho: %f" , correlationsFile.correlation(var1,binNum1,var2,binNum2) ) , kRed + 2 , 0.035 ) ;

	plotFile.draw() ;
	plotFile.canvas() -> Clear () ;
}


void PlottingUtils::plot2DCorrelations ( PlotFile & plotFile , TH2F * histo , const std::string & title , const float & lowerLimit , const float & upperLimit )
{
	if ( histo == NULL )
	{
		Utils::Error ( "PlottingUtils::plot2DCorrelations(...)" , "Provided NULL histogram - returning" ) ;
		return ;
	}

	plotFile.canvas () -> SetBottomMargin ( 0.26 ) ;
	plotFile.canvas () -> SetLeftMargin ( 0.2 ) ;
	plotFile.canvas () -> SetRightMargin ( 0.02 ) ;
	plotFile.canvas () -> SetTopMargin ( 0.08 ) ;

	gStyle -> SetPaintTextFormat ( "2.2f%" ) ;
	set2DCorrelationsColorPalette () ;
	histo -> GetXaxis () -> LabelsOption ( "v" ) ;
	histo -> GetXaxis () -> SetTitleOffset ( 1.8 ) ;
	histo -> GetYaxis () -> SetTitleOffset ( 1.6 ) ;
	histo -> GetZaxis () -> SetRangeUser ( lowerLimit , upperLimit ) ;

	if ( histo -> GetXaxis () -> GetNbins () < 14 )
	{
		histo -> SetMarkerSize ( 1.4 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.04 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.04 ) ;
	}
	else if ( histo -> GetXaxis () -> GetNbins () < 18 )
	{
		histo -> SetMarkerSize ( 1.0 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.035 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.035 ) ;
	}
	else  if ( histo -> GetXaxis () -> GetNbins () < 22 )
	{
		histo -> SetMarkerSize ( 0.8 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.03 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.03 ) ;
	}
	else  if ( histo -> GetXaxis () -> GetNbins () < 26 )
	{
		histo -> SetMarkerSize ( 0.7 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.025 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.025 ) ;
	}
	else  if ( histo -> GetXaxis () -> GetNbins () < 31 )
	{
		histo -> SetMarkerSize ( 0.6 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.025 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.025 ) ;
	}
	else  if ( histo -> GetXaxis () -> GetNbins () < 40 )
	{
		histo -> SetMarkerSize ( 0.5 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.02 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.02 ) ;
	}
	else
	{
		histo -> SetMarkerSize ( 0.4 ) ;
		histo -> GetXaxis () -> SetLabelSize ( 0.015 ) ;
		histo -> GetYaxis () -> SetLabelSize ( 0.015 ) ;
	}

	histo -> Draw ( "textcolor" ) ;
	PlottingUtils::Label ( 0.2 , 0.94 , title , kBlack , 0.04 ) ;

	plotFile.draw() ;
	plotFile.canvas () -> SetTopMargin ( 0.02 ) ;
	plotFile.canvas() -> Clear () ;
}


void PlottingUtils::plotSumYields ( PlotFile & plotFile , TH1F * histo , const float & yLower , const float & yUpper )
{
	histo -> GetYaxis () -> SetTitle ( "N_{entries}" ) ;
	histo -> GetYaxis () -> SetTitleOffset ( 1.8 ) ;
	histo -> GetYaxis () -> SetLabelSize ( 0.04 ) ;
	float yMin ( yLower < 0 ? 1e6 : yLower ) , yMax ( yUpper < 0 ? 0 : yUpper ) ;
	if ( yLower < 0 ) for ( int binNum (1) ; binNum <= histo -> GetNbinsX() ; ++ binNum ) if ( histo->GetBinContent(binNum) - histo->GetBinError(binNum) < yMin ) yMin = histo->GetBinContent(binNum) - histo->GetBinError(binNum) ;
	if ( yUpper < 0 ) for ( int binNum (1) ; binNum <= histo -> GetNbinsX() ; ++ binNum ) if ( histo->GetBinContent(binNum) + histo->GetBinError(binNum) > yMax ) yMax = histo->GetBinContent(binNum) + histo->GetBinError(binNum) ;
	if ( yLower < 0 ) yMin = yMin - 0.05 * ( yMax - yMin ) ;
	if ( yUpper < 0 ) yMax = yMax + 0.05 * ( yMax - yMin ) ;
	histo -> GetYaxis() -> SetRangeUser ( yMin , yMax ) ;
	histo -> GetXaxis() -> LabelsOption ( "v" ) ;
	histo -> GetXaxis () -> SetLabelSize ( 0.03 ) ;
	histo -> SetFillColorAlpha ( kAzure + 7 , 0.4 ) ;
	histo -> SetMarkerColor ( kAzure + 7 ) ;
	histo -> Draw ( "PE2" ) ;
	plotFile.draw() ;
}


void PlottingUtils::plotSumYieldResiduals ( PlotFile & plotFile , CorrelationsFile & correlationsFile , const std::string & referenceVar )
{
	if ( correlationsFile.sumYields() == NULL ) return ;
	TH1F * hDeltaYields = new TH1F ( * correlationsFile.sumYields() ) ;
	TH1F * hSigmaYields = new TH1F ( * correlationsFile.sumYields() ) ;
	float refYield ( correlationsFile.sumYield ( referenceVar ) ) ;
	float refErr ( correlationsFile.sumYieldError ( referenceVar ) ) ;
	int binNum1D_x ( 1 ) ;
	float yDeltaMin ( 1e6 ) , yDeltaMax ( -1e6 ) , ySigmaMin ( 1e6 ) , ySigmaMax ( -1e6 ) ;
	VecFloat pulls ;
	for ( const auto & var : correlationsFile.vars() )
	{
		float deltaYield ( correlationsFile.sumYield ( var ) - refYield ) ;
		float errVar ( correlationsFile.sumYieldError ( var ) ) ;
		float uncertainty ( sqrt ( errVar*errVar + refErr*refErr - 2 * correlationsFile.correlation( var , 1 , referenceVar , 1 )*refErr*errVar ) ) ;
		float pull ( deltaYield == 0 ? 0 : deltaYield / uncertainty ) ;
		hDeltaYields -> SetBinContent ( binNum1D_x , deltaYield ) ;
		hDeltaYields -> SetBinError ( binNum1D_x , uncertainty ) ;
		hSigmaYields -> SetBinContent ( binNum1D_x , pull ) ;
		pulls.push_back ( pull ) ;
		hDeltaYields -> GetXaxis() -> SetBinLabel ( binNum1D_x , correlationsFile.binLabel( var , 1 ).c_str() ) ;
		hSigmaYields -> GetXaxis() -> SetBinLabel ( binNum1D_x , correlationsFile.binLabel( var , 1 ).c_str() ) ;
		if ( deltaYield - uncertainty < yDeltaMin ) yDeltaMin = deltaYield - uncertainty ;
		if ( deltaYield + uncertainty > yDeltaMax ) yDeltaMax = deltaYield + uncertainty ;
		if ( deltaYield / uncertainty < ySigmaMin ) ySigmaMin = deltaYield / uncertainty ;
		if ( deltaYield / uncertainty > ySigmaMax ) ySigmaMax = deltaYield / uncertainty ;
		++ binNum1D_x ;
	}

	hDeltaYields -> GetXaxis() -> LabelsOption ( "v" ) ;
	hDeltaYields -> GetXaxis () -> SetLabelSize ( 0.03 ) ;
	hDeltaYields -> SetFillColorAlpha ( kAzure + 7 , 0.4 ) ;
	hDeltaYields -> SetMarkerColor ( kAzure + 7 ) ;
	hDeltaYields -> GetYaxis () -> SetTitle ( Form("N_{entries}^{var} - N_{entries}^{%s}",correlationsFile.binLabel( referenceVar , 1 ).c_str()) ) ;
	hDeltaYields -> GetYaxis () -> SetLabelSize ( 0.04 ) ; hDeltaYields -> GetYaxis () -> SetTitleSize ( 0.04 ) ; hSigmaYields -> GetYaxis () -> SetTitleOffset ( 2.0 ) ;
	hDeltaYields -> GetYaxis() -> SetRangeUser ( yDeltaMin - 0.05 * ( yDeltaMax - yDeltaMin ) , yDeltaMax + 0.05 * ( yDeltaMax - yDeltaMin ) ) ;
	hDeltaYields -> Draw ( "PE2" ) ;
	TF1 * lineAtZero = new TF1 ( "lineAtZero" , "0" , 0 , hDeltaYields -> GetNbinsX () ) ;
	lineAtZero -> SetLineWidth ( 1 ) ; lineAtZero -> SetLineStyle ( 2 ) ; lineAtZero -> SetLineColor ( kBlack ) ;
	lineAtZero -> Draw ( "same" ) ;
	plotFile.draw() ;

	hSigmaYields -> GetXaxis() -> LabelsOption ( "v" ) ;
	hSigmaYields -> GetXaxis () -> SetLabelSize ( 0.03 ) ;
	hSigmaYields -> SetFillColorAlpha ( kAzure + 7 , 0.4 ) ;
	hSigmaYields -> GetYaxis () -> SetTitle ( Form("#frac{N_{entries}^{var} - N_{entries}^{%s}}{#Delta N_{entries}}",correlationsFile.binLabel( referenceVar , 1 ).c_str()) ) ;
	hSigmaYields -> GetYaxis () -> SetLabelSize ( 0.04 ) ; hSigmaYields -> GetYaxis () -> SetTitleSize ( 0.04 ) ; hSigmaYields -> GetYaxis () -> SetTitleOffset ( 2.0 ) ;
	hSigmaYields -> GetYaxis() -> SetRangeUser ( ySigmaMin - 0.05 * ( ySigmaMax - ySigmaMin ) , ySigmaMax + 0.05 * ( ySigmaMax - ySigmaMin ) ) ;
	hSigmaYields -> SetLineWidth ( 0 ) ;
	hSigmaYields -> Sumw2 ( kFALSE ) ;
	hSigmaYields -> Draw ( "HIST" ) ;
	std::vector < TF1 * > lines ;
	for ( int i (-10) ; i <= 10 ; ++ i )
	{
		TF1 * lineAtHere = new TF1 ( Form("lineAt_%d",i) , Form("%d",i) , 0 , hSigmaYields -> GetNbinsX () ) ;
		lineAtHere -> SetLineWidth ( 1 ) ; lineAtHere -> SetLineStyle ( 2 ) ; lineAtHere -> SetLineColor ( kBlack ) ;
		lineAtHere -> Draw ( "same" ) ;
		lines.push_back ( lineAtHere ) ;
	}
	plotFile.draw() ;

	delete hDeltaYields ;
	delete hSigmaYields ;
	delete lineAtZero ;
	for ( TF1 * & line : lines ) delete line ;

	if ( pulls.size () < 2 ) return ;
	std::sort ( pulls.begin () , pulls.end () ) ;
	float pulls_range ( pulls.back() - pulls.front() ) ;
	TH1F * hPulls = new TH1F ( "PlottingUtils_plotSumYieldResiduals_pulls" , "" , 10 * ceil(1.1*pulls_range) , pulls.front() - 0.05*pulls_range , pulls.back() + 0.05*pulls_range ) ;
	hPulls -> Sumw2 ( kFALSE ) ;
	for ( const float & pull : pulls ) hPulls -> Fill ( pull ) ;
	hPulls -> GetXaxis () -> SetTitle ( Form("Pull wrt %s",correlationsFile.binLabel( referenceVar , 1 ).c_str()) ) ;
	hPulls -> GetXaxis () -> SetLabelSize ( 0.04 ) ; hPulls -> GetXaxis () -> SetTitleSize ( 0.04 ) ; hPulls -> GetXaxis () -> SetTitleOffset ( 2.2 ) ;
	hPulls -> GetYaxis () -> SetTitle ( "Normalised num vars" ) ;
	hPulls -> GetYaxis () -> SetLabelSize ( 0.04 ) ; hPulls -> GetYaxis () -> SetTitleSize ( 0.04 ) ; hPulls -> GetYaxis () -> SetTitleOffset ( 2.0 ) ;
	hPulls -> Scale ( 1.0 / hPulls -> Integral () ) ;
	hPulls -> SetFillColorAlpha ( kAzure + 7 , 0.4 ) ;
	hPulls -> SetLineWidth ( 0 ) ;
	float histoHeight ( 1.4 * PlottingUtils::getHistoMax ( hPulls , false ) ) ;
	hPulls -> GetYaxis() -> SetRangeUser ( 0. , histoHeight ) ;
	hPulls -> Draw ( "HIST" ) ;

	TLine verticalLineAtZero ( 0 , 0 , 0 , histoHeight ) ;
	verticalLineAtZero.SetLineWidth ( 1 ) ; verticalLineAtZero.SetLineStyle ( 2 ) ;
	verticalLineAtZero.Draw ( "same" ) ;

	TF1 * fGauss = new TF1 ( "fGauss" , Form("TMath::Gaus(x,[0],[1])/%f",hPulls->GetEntries()) , pulls.front()-0.05*pulls_range , pulls.back()+0.05*pulls_range ) ;
	fGauss -> SetParameter ( 0 , hPulls -> GetMean () ) ; fGauss -> SetParameter ( 1 , hPulls -> GetRMS () ) ;
	fGauss -> SetLineColor ( kRed + 2 ) ; fGauss -> SetLineWidth ( 1 ) ; fGauss -> Draw ( "same" ) ;

	PlottingUtils::Label ( 0.28 , 0.89 , Form ( "Mean: %0.3f #pm %0.3f" , hPulls -> GetMean () , hPulls -> GetMeanError () ) , kRed + 2 , 0.035 ) ;
	PlottingUtils::Label ( 0.28 , 0.84 , Form ( "RMS: %0.3f #pm %0.3f" , hPulls -> GetRMS () , hPulls -> GetRMSError () ) , kRed + 2 , 0.035 ) ;

	plotFile.draw () ;
	delete hPulls ;
	delete fGauss ;
}


void PlottingUtils::writeCorrelationsToFile ( CorrelationsFile & correlationsFile , const std::string & filename )
{
	std::ofstream outFile ( filename ) ;
	if ( ! outFile.is_open() ) Utils::Fatal ( "PlottingUtils::writeCorrelationsToFile()" , "File " + filename + " could not be opened" ) ;

	outFile << "Variables:" ;
	for ( const std::string & var : correlationsFile.vars() ) outFile << " " << var ;
	outFile << std::endl << std::endl ;

	for ( const std::string & var : correlationsFile.vars() )
	{
		outFile << var << ".nBins: " << correlationsFile.nBins ( var ) << std::endl ;
		for ( unsigned int binNum ( 1 ) ; binNum <= correlationsFile.nBins ( var ) ; ++binNum )
		{
			outFile << var << ".bin" << binNum << ".label: " << correlationsFile.binLabel ( var , binNum ) << std::endl ;
		}
		outFile << std::endl ;
	}

	for ( const std::string & var1 : correlationsFile.vars() )
	{
		for ( unsigned int binNum1 ( 1 ) ; binNum1 <= correlationsFile.nBins ( var1 ) ; ++binNum1 )
		{
			for ( const std::string & var2 : correlationsFile.vars() )
			{
				for ( unsigned int binNum2 ( 1 ) ; binNum2 <= correlationsFile.nBins ( var2 ) ; ++binNum2 )
				{
					outFile << "Correlation." << var1 << ".bin" << binNum1 << "." << var2 << ".bin" << binNum2 << ": " << correlationsFile.correlation ( var1 , binNum1 , var2 , binNum2 ) << std::endl ;
					outFile << "Significance." << var1 << ".bin" << binNum1 << "." << var2 << ".bin" << binNum2 << ": " << correlationsFile.corrSignificance ( var1 , binNum1 , var2 , binNum2 ) << std::endl << std::endl ;
				}
			}
		}
	}

	outFile.close () ;
}


void PlottingUtils::makeCorrelationsTable ( CalculateCorrelationsConfig & config , CorrelationsFile & correlationsFile , const std::string & filename )
{
	std::ofstream outFile ( filename ) ;
	if ( ! outFile.is_open() ) Utils::Fatal ( "PlottingUtils::makeCorrelationsTable()" , "File " + filename + " could not be opened" ) ;

	const VecStr & targetVars ( config.corrMatrixVariables().size() > 0 ? config.corrMatrixVariables() : correlationsFile.vars() ) ;
	for ( const std::string & var : targetVars )
	{
		if ( ! correlationsFile.hasVar ( var ) ) Utils::Fatal ( "PlottingUtils::writeCorrelationMatrixToFile(...)" , "Requested variable " + var + " not found" ) ;
	}

	for ( const std::string & var_x : targetVars )
	{
		for ( const std::string & var_y : targetVars )
		{
			if ( var_x == var_y ) continue ;
			if ( correlationsFile.nBins ( var_x ) > correlationsFile.nBins ( var_y ) ) continue ;
			outFile << "\\begin{table}[!htb]\n\\begin{tabular}{lcccccccccccc}" << std::endl ;
			for ( unsigned int binNum_x ( 1 ) ; binNum_x <= correlationsFile.nBins ( var_x ) ; ++binNum_x )
			{
				TString binLabel = TString( Form("$ %s $",correlationsFile.binLabel ( var_x , binNum_x ).c_str()) ).ReplaceAll("#","\\") ;
				outFile << " & " << binLabel.Data() ;
			}
			outFile << "\\\\\n\\hline" << std::endl ;

			for ( unsigned int binNum_y ( 1 ) ; binNum_y <= correlationsFile.nBins ( var_y ) ; ++binNum_y )
			{
				TString binLabel = TString( Form("$ %s $",correlationsFile.binLabel ( var_y , binNum_y ).c_str()) ).ReplaceAll("#","\\") ;
				outFile << binLabel.Data() ;
				for ( unsigned int binNum_x ( 1 ) ; binNum_x <= correlationsFile.nBins ( var_x ) ; ++binNum_x )
				{
					float corr ( correlationsFile.correlation ( var_x , binNum_x , var_y , binNum_y ) ) ;
					float upErr ( correlationsFile.corrError_up ( var_x , binNum_x , var_y , binNum_y ) - correlationsFile.correlation ( var_x , binNum_x , var_y , binNum_y ) ) ;
					float dwnErr ( correlationsFile.correlation ( var_x , binNum_x , var_y , binNum_y ) - correlationsFile.corrError_down ( var_x , binNum_x , var_y , binNum_y ) ) ;
					outFile << " & " << Form ( "$%.2f^{+%.2f}_{%.2f}$" , corr , upErr , dwnErr ) ;
				}
				outFile << "\\\\" << std::endl ;
			}
			outFile << "\\end{tabular}\n\\end{table}\n\n\n\n" ;
		}
	}
	outFile.close () ;
}


void PlottingUtils::writeCorrelationMatrixToFile ( CalculateCorrelationsConfig & config , CorrelationsFile & correlationsFile , const std::string & filename )
{
	std::ofstream outFile ( filename ) ;
	if ( ! outFile.is_open() ) Utils::Fatal ( "PlottingUtils::writeCorrelationsToFile()" , "File " + filename + " could not be opened" ) ;

	auto now ( std::chrono::system_clock::now() ) ;
	auto in_time_t ( std::chrono::system_clock::to_time_t(now) ) ;

	outFile << "##################################################################" << std::endl ;
	outFile << "##   Automatically generated correlation matrix" << std::endl ;
//	outFile << "##   File produced on " << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d at %X") << std::endl ;
	outFile << "##   " << ( config.corrMatrixZeroDiagonal() ? "S" : "Not s" ) << "etting diagonal elements to zero" << std::endl ;
	outFile << "##   " << ( config.corrMatrixIgnoreUnderflow() ? "I" : "Not i" ) << "gnoring underflow bins" << std::endl ;
	outFile << "##   " << ( config.corrMatrixIgnoreOverflow() ? "I" : "Not i" ) << "gnoring overflow bins" << std::endl ;
	outFile << "##   " << ( config.corrMatrixZeroSelfCorrelation() ? "S" : "Not s" ) << "etting variable self-correlations to zero" << std::endl ;
	outFile << "##################################################################" << std::endl << std::endl ;

	outFile << "Variables:" ;
	const VecStr & targetVars ( config.corrMatrixVariables().size() > 0 ? config.corrMatrixVariables() : correlationsFile.vars() ) ;
	for ( const std::string & var : targetVars )
	{
		if ( ! correlationsFile.hasVar ( var ) ) Utils::Fatal ( "PlottingUtils::writeCorrelationMatrixToFile(...)" , "Requested variable " + var + " not found" ) ;
		outFile << " " << var ;
	}
	outFile << std::endl << std::endl ;

	std::map < std::string , VecInt > targetBinNums ;
	for ( const std::string & var : targetVars )
	{
		outFile << var << ".nBins: " << correlationsFile.nBins ( var ) << std::endl ;
		for ( unsigned int binNum ( 1 ) ; binNum <= correlationsFile.nBins ( var ) ; ++binNum )
		{
			std::string binLabel ( correlationsFile.binLabel ( var , binNum ) ) ;
			if ( config.corrMatrixIgnoreUnderflow() && binLabel.find ( "nderflow" ) != std::string::npos ) continue ;
			if ( config.corrMatrixIgnoreOverflow()  && binLabel.find ( "verflow"  ) != std::string::npos ) continue ;
			outFile << var << ".bin" << binNum << ".label: " << binLabel << std::endl ;
			targetBinNums [ var ] . push_back ( binNum ) ;
		}
		outFile << std::endl ;
	}

	std::string matrixName ( "CorrelationMatrix" ) ;
	bool isFirstLine ( true ) ;
	for ( const std::string & var1 : targetVars )
		for ( const int & binNum1 : targetBinNums [ var1 ] )
		{
			outFile << ( isFirstLine ? "" : "+" ) << matrixName << ":" ;
			isFirstLine = false ;
			for ( const std::string & var2 : targetVars )
				for ( const int & binNum2 : targetBinNums [ var2 ] )
					if ( config.corrMatrixZeroDiagonal() && var1 == var2 && binNum1 == binNum2 ) outFile << " 0" ;
					else if ( config.corrMatrixZeroSelfCorrelation() && var1 == var2 ) outFile << " 0" ;
					else outFile << " " << correlationsFile.correlation ( var1 , binNum1 , var2 , binNum2 ) ;
			outFile << std::endl ;
		}
	outFile << std::endl ;

	matrixName = "CorrelationMatrixErrorUp" ;
	isFirstLine = true ;
	for ( const std::string & var1 : targetVars )
		for ( const int & binNum1 : targetBinNums [ var1 ] )
		{
			outFile << ( isFirstLine ? "" : "+" ) << matrixName << ":" ;
			isFirstLine = false ;
			for ( const std::string & var2 : targetVars )
				for ( const int & binNum2 : targetBinNums [ var2 ] )
					if ( config.corrMatrixZeroSelfCorrelation() && var1 == var2 ) outFile << " 0" ;
					else outFile << " " << correlationsFile.corrError_up ( var1 , binNum1 , var2 , binNum2 ) - correlationsFile.correlation ( var1 , binNum1 , var2 , binNum2 ) ;
			outFile << std::endl ;
		}
	outFile << std::endl ;

	matrixName = "CorrelationMatrixErrorDown" ;
	isFirstLine = true ;
	for ( const std::string & var1 : targetVars )
		for ( const int & binNum1 : targetBinNums [ var1 ] )
		{
			outFile << ( isFirstLine ? "" : "+" ) << matrixName << ":" ;
			isFirstLine = false ;
			for ( const std::string & var2 : targetVars )
				for ( const int & binNum2 : targetBinNums [ var2 ] )
					if ( config.corrMatrixZeroSelfCorrelation() && var1 == var2 ) outFile << " 0" ;
					else outFile << " " << correlationsFile.corrError_down ( var1 , binNum1 , var2 , binNum2 ) - correlationsFile.correlation ( var1 , binNum1 , var2 , binNum2 ) ;
			outFile << std::endl ;
		}

	outFile << "\n##################################################################" << std::endl ;
	outFile << "##  End of file" << std::endl ;
	outFile << "##################################################################" << std::endl ;

	outFile.close () ;
}


BinnedChi2Holder PlottingUtils::plotBkgFit ( PlotFile & plotFile , Punchcard & punchcard , Fitter & fitter , const float & lumi , const std::string & ATLASLabel , const bool & sidebandOnly , const float & signalLower , const float & signalUpper , const float & binWidth )
{
	BinnedChi2Holder to_return ;
	RooRealVar * myy ( fitter.fitVar() ) ;
	RooDataSet * dataset ( (RooDataSet*) fitter.lastRooDataset() ) ;
	if ( sidebandOnly && signalLower > 0 && signalUpper > 0 && signalUpper > signalLower )
	{
		RooThresholdCategory sidebandRegion ( "SidebandRegion" , "Sideband region" , * myy , "Sideband" ) ;
		sidebandRegion.addThreshold ( signalLower , "Sideband" ) ;
		sidebandRegion.addThreshold ( signalUpper , "Signal" ) ;
		RooThresholdCategory sidebandRegion2 ( "SidebandRegion2" , "Sideband region 2" , * myy , "SidebandUpper" ) ;
		sidebandRegion2.addThreshold ( signalLower , "SidebandLower" ) ;
		sidebandRegion2.addThreshold ( signalUpper , "Signal" ) ;
		dataset -> addColumn ( sidebandRegion ) ;
		dataset -> addColumn ( sidebandRegion2 ) ;
	}
	RooAbsPdf * pdf ( fitter.lastPdf() /*fitter.workspace() -> pdf ( sidebandOnly ? "extended_bkg_data_pdf" : "sig_bkg_data_pdf" )*/ ) ;
	std::string pdfName ( pdf -> GetName() ) ;
	if ( pdfName.find ( "data_" ) != std::string::npos ) pdfName.replace ( pdfName.find("data_") , 5 , "" ) ;
	RooCategory * data_cat ( fitter.dataCategory() ) ;
	bool containsSignal ( pdfName.find ( "sig" ) != std::string::npos ) ;
	plotFile.cd() ;
	for ( unsigned int binNum ( 1 ) ; binNum <= fitter.nBins() ; ++binNum )
	{
		RooPlot * frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;

		double correction ( 1 ) ;
		if ( sidebandOnly )
		{
	    		RooAbsReal * integralFull = fitter.workspace() -> pdf ( Form("bkg_pdf_bin%d",binNum) ) -> createIntegral ( * myy , Range("FullRange") ) ;
			RooAbsReal * integralSidebandLower = fitter.workspace() -> pdf (Form("bkg_pdf_bin%d",binNum) ) -> createIntegral ( * myy , Range("SidebandLower") ) ;
			RooAbsReal * integralSidebandUpper = fitter.workspace() -> pdf ( Form("bkg_pdf_bin%d",binNum) ) -> createIntegral ( * myy , Range("SidebandUpper") ) ;
			double sumEntries ( dataset -> sumEntries ( Form("sample == sample::data_bin%d",binNum) ) ) ;
			double sumEntriesSideband ( dataset -> sumEntries ( Form("SidebandRegion == SidebandRegion::Sideband && sample == sample::data_bin%d",binNum) ) ) ;
			correction = ( integralSidebandLower -> getVal() + integralSidebandUpper -> getVal() ) / integralFull -> getVal() ;
			correction *= ( sumEntries / sumEntriesSideband ) ;
		}

		int itrFactor ( 1 ) ;
		float lastBinEntries ( 31 ) ;
		int nPlotBins ( ( myy->getMax() - myy->getMin() ) / binWidth ) ;
		if ( binWidth <= 0 ) while ( lastBinEntries > 30 ) {
			++ itrFactor ;
			nPlotBins = itrFactor * ( myy->getMax() - myy->getMin() ) ;
			float width ( ( myy->getMax() - myy->getMin() ) / nPlotBins ) ;
			lastBinEntries = dataset -> sumEntries ( Form("sample == sample::data_bin%d && m_yy > %f",binNum,myy->getMax()-width) ) ;
		}

	 	if ( sidebandOnly ) dataset -> plotOn(frame,DataError(RooAbsData::SumW2),LineColor(12), Binning(nPlotBins,myy->getMin(),myy->getMax()), Cut(Form("sample == sample::data_bin%d && SidebandRegion==SidebandRegion::Sideband",binNum)) ) ;
		else dataset -> plotOn(frame,DataError(RooAbsData::SumW2),LineColor(12), Cut(Form("sample == sample::data_bin%d",binNum)) , Binning(nPlotBins,myy->getMin(),myy->getMax()) ) ;
		TGraph * graph_data = (TGraph*) frame -> getObject ( frame -> numItems() - 1 ) ;

		if ( sidebandOnly )
		{	   	
			if ( containsSignal ) pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure) , LineStyle(2) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) , Range("FullRange") , NormRange("FullRange") ) ;
			else pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure) , LineStyle(2) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) , Range("FullRange") , NormRange("FullRange") , Normalization(1.0/correction) ) ;
	   	}
		else pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure) , LineStyle(2) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) , Range("FullRange") , NormRange("FullRange") ) ;
		TGraph * graph_bkg = (TGraph*) frame -> getObject ( frame -> numItems() - 1  ) ;
		frame -> GetYaxis() -> SetTitle ( Form("Events / %.2f GeV", (myy->getMax() - myy->getMin()) / nPlotBins) ) ;
		frame -> SetMaximum ( 1.2 * frame -> GetMaximum () ) ;
		frame -> Draw () ;
		PlottingUtils::ATLAS_LABEL(0.65,0.87,1);
		PlottingUtils::DrawText2 ( ATLASLabel.c_str() , kBlack , 0.87 , 0.765 , 0.05 ) ;
		PlottingUtils::DrawText2 ( punchcard.getBinLabel(binNum).c_str() , kBlack , 0.816 , 0.65 , 0.037 ) ;
		PlottingUtils::DrawText2 ( Form("#sqrt{s} = 13 TeV, %.1f fb^{-1}",0.001*lumi) , kBlack , 0.76 , 0.65 , 0.037 ) ;
		PlottingUtils::DrawText2 ( Form("Bkg: %s",BackgroundModel::name(fitter.bkgModelType(binNum)).c_str()) , kBlack , 0.704 , 0.65 , 0.037 ) ;
		if ( sidebandOnly ) PlottingUtils::DrawText2 ( "Sideband only" , kBlack , 0.65 , 0.65 , 0.037 ) ;

		// Draw a sub pad to add a ratio plot
		PlottingUtils::DrawSubPad();
		RooHist * h_ratio ( NULL ) ;
		if ( sidebandOnly ) h_ratio = frame -> getHist ( Form("h_c_data_ds_Cut[sample == sample::data_bin%d && SidebandRegion==SidebandRegion::Sideband]"/*"h_c_data_ds_Cut[sample == sample::data_bin%d]"*/,binNum) ) ;
		else h_ratio = frame -> getHist ( Form("h_c_data_ds_Cut[sample == sample::data_bin%d]",binNum) ) ;
		RooCurve * c_bkg_pdf ( frame -> getCurve ( Form("%s_bin%d_Norm[m_yy]_Comp[bkg_pdf_bin%d]_Range[FullRange]_NormRange[FullRange]",pdfName.c_str(),binNum,binNum) ) ) ;
		TH1 * h_res = NULL ;
		h_res = Utils::residualHist ( h_ratio , c_bkg_pdf , false ) ;
		h_res -> SetLineColor ( 12 ) ;
		h_res -> GetXaxis() -> SetTitle ( "#scale[1.2]{m_{ #gamma#gamma} [GeV]}" ) ;
		float yMax ( 0 ) , yMin ( 0 ) ;
		unsigned int nFilledBins ( 0 ) ;
		for ( int binNum ( 1 ) ; binNum <= h_res -> GetNbinsX() ; ++ binNum )
		{
			float thisMin ( h_res -> GetBinContent ( binNum ) - h_res -> GetBinError ( binNum ) ) ;
			float thisMax ( h_res -> GetBinContent ( binNum ) + h_res -> GetBinError ( binNum ) ) ;
			if ( thisMin < yMin ) yMin = thisMin ;
			if ( thisMax > yMax ) yMax = thisMax ;
			if ( thisMin != thisMax ) ++ nFilledBins ;
		}
		float range ( yMax - yMin ) ;
		h_res -> GetYaxis() -> SetRangeUser ( yMin - 0.15 * range , yMax + 0.15 * range ) ;
		h_res -> DrawCopy() ;
		TF1 * lineAtZero = new TF1 ( "lineAtZero" , "0" , myy->getMin() , myy->getMax() ) ;
		lineAtZero -> SetLineWidth ( 2 ) ; lineAtZero -> SetLineStyle ( 2 ) ; lineAtZero -> SetLineColor ( kAzure ) ;
		lineAtZero -> Draw ( "same" ) ;

		to_return.addBin ( binNum , punchcard.getBinLabel(binNum) , BackgroundModel::name(fitter.bkgModelType(binNum)) , Utils::chi2 ( h_res ) , nFilledBins - BackgroundModel::nDoF(fitter.bkgModelType(binNum)) ) ;
		
		frame -> Print ( "v" ) ;
		plotFile.draw () ;
	}
	return to_return ;
}


void PlottingUtils::quickFitPlot ( PlotFile & plotFile , Fitter & fitter , const std::string & text )
{
	RooRealVar * myy ( fitter.fitVar() ) ;
	RooAbsData * dataset ( (RooAbsData*) fitter.lastRooDataset() ) ;
	RooAbsPdf * pdf ( fitter.lastPdf() ) ;
	RooCategory * data_cat ( fitter.dataCategory() ) ;
	plotFile.cd() ;
	for ( unsigned int binNum ( 1 ) ; binNum <= fitter.nBins() ; ++binNum )
	{
	    RooPlot * frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;
	    dataset -> plotOn(frame,DataError(RooAbsData::SumW2),LineColor(12), Cut(Form("sample == sample::data_bin%d",binNum)) ) ;
	    TGraph * graph_data = (TGraph*) frame -> getObject ( frame -> numItems() - 1 ) ;
	    pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("bkg_pdf_bin%d",binNum)) , LineColor(kAzure) , LineStyle(2) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) ) ;
	    TGraph * graph_bkg = (TGraph*) frame -> getObject ( frame -> numItems() - 1  ) ;
	    pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , LineColor(kRed) , LineStyle(1) , LineWidth(2) , ProjWData(RooArgSet(*data_cat), * dataset) ) ;
	    TGraph * graph_fit = (TGraph*) frame -> getObject ( frame -> numItems() - 1  );
	    pdf -> plotOn ( frame , LineColor(12) , Slice(*data_cat,Form("data_bin%d",binNum)) , Components(Form("sig_pdf_bin%d",binNum)) , LineColor(kBlack) , LineStyle(1) , LineWidth(2) , ProjWData(RooArgSet(*data_cat),*dataset) ) ;
	    TGraph * graph_sig = (TGraph*) frame -> getObject ( frame -> numItems() - 1  );
	    frame -> GetYaxis() -> SetTitle ( "Events / bin width" ) ;
	    frame -> SetMaximum ( 1.2 * frame -> GetMaximum () ) ;
	    frame -> Draw () ;
		if ( text.size() > 0 ) Label ( 0.4 , 0.85 , TString(text.c_str()).ReplaceAll("BIN",Form("%d",binNum)).Data() , kBlack , 0.035 ) ;
	    plotFile.draw () ;
	}
}


void PlottingUtils::quickFitPlot ( PlotFile & plotFile , RooRealVar * myy , RooAbsData * data , RooAbsPdf * pdf , const std::string & text )
{
	plotFile.cd() ;
	RooPlot * frame = myy -> frame ( Title("m_{#gamma #gamma} [GeV]") ) ;
	data -> plotOn( frame , DataError(RooAbsData::SumW2) , LineColor(kBlack) ) ;
	TGraph * graph_data = (TGraph*) frame -> getObject ( frame -> numItems() - 1 ) ;
	pdf -> plotOn ( frame , LineColor(kAzure) , LineStyle(2) , LineWidth(4) ) ;
	TGraph * graph_bkg = (TGraph*) frame -> getObject ( frame -> numItems() - 1  ) ;
	frame -> GetYaxis() -> SetTitle ( "Events / bin width" ) ;
	frame -> SetMaximum ( 1.2 * frame -> GetMaximum () ) ;
	frame -> Draw () ;
	if ( text.size() > 0 ) Label ( 0.4 , 0.85 , text , kBlack , 0.035 ) ;
	plotFile.draw () ;
}
