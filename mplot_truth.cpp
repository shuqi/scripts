#include <SampleHandler/ScanDir.h>
#include <SampleHandler/SampleHandler.h>
void mplot_truth()
{
  std::string path = "/afs/cern.ch/user/s/shuqi/eos/Zy_signal_MC_Sh2211/user.shuqi.mc16e_700398_mumugamma_JetSyst_ANALYSIS.root";
  SH::SampleHandler sh;
  SH::ScanDir().sampleDepth(0).scan(sh, path);
  sh.print ();
  
  std::vector<std::string> files;
  std::cout<<"files size = "<<sh.size()<<endl;
  //for (unsigned iter = 0; iter != sh.size(); ++ iter)
  for (unsigned iter = 0; iter != sh.size(); ++ iter)
  {
    SH::Sample *sample = sh.at (iter);
    std::cout<<iter<<endl;
    std::cout<<"Processing sample:"<<sample->name()<<endl;
    std::string fileName = sample->name();
    std::string SampleName = path + "/" + fileName.c_str();
    //std::cout<<SampleName<<endl;
    
    TFile *file = new TFile(SampleName.c_str(),"read");
    
    std::string OutName = "../CS_Info_sig_mc16e_mumugamma_JetSyst/"+fileName;
    TFile *f = new TFile(OutName.c_str(), "RECREATE");

    std::vector<std::string> syst_list;
    syst_list.push_back("NOMINAL");
    /*
    syst_list.push_back("MUON_MS__1up");
    syst_list.push_back("MUON_MS__1down");
    syst_list.push_back("MUON_ID__1up");
    syst_list.push_back("MUON_ID__1down");
    syst_list.push_back("MUON_SCALE__1up");
    syst_list.push_back("MUON_SCALE__1down");
    syst_list.push_back("MUON_SAGITTA_RHO__1up");
    syst_list.push_back("MUON_SAGITTA_RHO__1down");
    syst_list.push_back("MUON_EFF_ISO_SYS__1up");
    syst_list.push_back("MUON_EFF_ISO_SYS__1down");
    syst_list.push_back("MUON_EFF_RECO_SYS__1up");
    syst_list.push_back("MUON_EFF_RECO_SYS__1down");
    syst_list.push_back("MUON_EFF_RECO_STAT__1up");
    syst_list.push_back("MUON_EFF_RECO_STAT__1down"); 
    syst_list.push_back("MUON_EFF_ISO_STAT__1up");
    syst_list.push_back("MUON_EFF_ISO_STAT__1down");
    syst_list.push_back("MUON_EFF_TTVA_SYS__1up");
    syst_list.push_back("MUON_EFF_TTVA_SYS__1down");
    syst_list.push_back("MUON_EFF_TTVA_STAT__1up");
    syst_list.push_back("MUON_EFF_TTVA_STAT__1down");
    syst_list.push_back("MUON_SAGITTA_RESBIAS__1up");
    syst_list.push_back("MUON_SAGITTA_RESBIAS__1down");
    syst_list.push_back("MUON_EFF_RECO_SYS_LOWPT__1up");
    syst_list.push_back("MUON_EFF_RECO_SYS_LOWPT__1down");
    syst_list.push_back("MUON_EFF_RECO_STAT_LOWPT__1up");
    syst_list.push_back("MUON_EFF_RECO_STAT_LOWPT__1down");
    syst_list.push_back("PRW_DATASF__1up");
    syst_list.push_back("PRW_DATASF__1down");
    syst_list.push_back("PH_EFF_ISO_Uncertainty__1up");
    syst_list.push_back("PH_EFF_ISO_Uncertainty__1down");
    syst_list.push_back("PH_EFF_TRIGGER_Uncertainty__1up");
    syst_list.push_back("PH_EFF_TRIGGER_Uncertainty__1down");
    syst_list.push_back("PH_EFF_ID_Uncertainty__1up");
    syst_list.push_back("PH_EFF_ID_Uncertainty__1down");
    syst_list.push_back("EG_SCALE_ALL__1up");
    syst_list.push_back("EG_SCALE_ALL__1down");
    syst_list.push_back("EG_SCALE_AF2__1up");
    syst_list.push_back("EG_SCALE_AF2__1down");
    syst_list.push_back("EG_RESOLUTION_ALL__1up");
    syst_list.push_back("EG_RESOLUTION_ALL__1down");
    syst_list.push_back("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up");
    syst_list.push_back("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down");
    syst_list.push_back("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up");
    syst_list.push_back("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down");
    syst_list.push_back("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up");
    syst_list.push_back("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down");
    */
    syst_list.push_back("JET_JvtEfficiency__1up");
    syst_list.push_back("JET_JvtEfficiency__1down");
    syst_list.push_back("JET_Pileup_PtTerm__1up");
    syst_list.push_back("JET_Pileup_PtTerm__1down");
    syst_list.push_back("JET_BJES_Response__1up");
    syst_list.push_back("JET_BJES_Response__1down");
    syst_list.push_back("JET_fJvtEfficiency__1up");
    syst_list.push_back("JET_fJvtEfficiency__1down");
    syst_list.push_back("JET_Flavor_Response__1up");
    syst_list.push_back("JET_Flavor_Response__1down");
    syst_list.push_back("JET_Pileup_OffsetMu__1up");
    syst_list.push_back("JET_Pileup_OffsetMu__1down");
    syst_list.push_back("JET_Pileup_OffsetNPV__1up");
    syst_list.push_back("JET_Pileup_OffsetNPV__1down");
    syst_list.push_back("JET_JER_EffectiveNP_3__1up");
    syst_list.push_back("JET_JER_EffectiveNP_3__1down");
    syst_list.push_back("JET_JER_DataVsMC_MC16__1up");
    syst_list.push_back("JET_JER_DataVsMC_MC16__1down");
    syst_list.push_back("JET_PunchThrough_MC16__1up");
    syst_list.push_back("JET_PunchThrough_MC16__1down");
    syst_list.push_back("JET_JER_EffectiveNP_7__1up");
    syst_list.push_back("JET_JER_EffectiveNP_7__1down");
    syst_list.push_back("JET_JER_EffectiveNP_9__1up");
    syst_list.push_back("JET_JER_EffectiveNP_9__1down");
    syst_list.push_back("JET_JER_EffectiveNP_6__1up");
    syst_list.push_back("JET_JER_EffectiveNP_6__1down");
    syst_list.push_back("JET_JER_EffectiveNP_8__1up");
    syst_list.push_back("JET_JER_EffectiveNP_8__1down");
    syst_list.push_back("JET_JER_EffectiveNP_4__1up");
    syst_list.push_back("JET_JER_EffectiveNP_4__1down");
    syst_list.push_back("JET_JER_EffectiveNP_5__1up");
    syst_list.push_back("JET_JER_EffectiveNP_5__1down");
    syst_list.push_back("JET_JER_EffectiveNP_2__1up");
    syst_list.push_back("JET_JER_EffectiveNP_2__1down");
    syst_list.push_back("JET_JER_EffectiveNP_1__1up");
    syst_list.push_back("JET_JER_EffectiveNP_1__1down");
    syst_list.push_back("JET_EffectiveNP_Mixed2__1up");
    syst_list.push_back("JET_EffectiveNP_Mixed2__1down");
    syst_list.push_back("JET_EffectiveNP_Mixed1__1up");
    syst_list.push_back("JET_EffectiveNP_Mixed1__1down");
    syst_list.push_back("JET_EffectiveNP_Mixed3__1up");
    syst_list.push_back("JET_EffectiveNP_Mixed3__1down");
    syst_list.push_back("JET_JER_EffectiveNP_11__1up");
    syst_list.push_back("JET_JER_EffectiveNP_11__1down");
    syst_list.push_back("JET_JER_EffectiveNP_10__1up");
    syst_list.push_back("JET_JER_EffectiveNP_10__1down");
    syst_list.push_back("JET_Pileup_RhoTopology__1up");
    syst_list.push_back("JET_Pileup_RhoTopology__1down");
    syst_list.push_back("JET_Flavor_Composition__1up");
    syst_list.push_back("JET_Flavor_Composition__1down");
    syst_list.push_back("JET_EffectiveNP_Detector2__1up");
    syst_list.push_back("JET_EffectiveNP_Detector2__1down");
    syst_list.push_back("JET_EffectiveNP_Detector1__1up");
    syst_list.push_back("JET_EffectiveNP_Detector1__1down");
    syst_list.push_back("JET_SingleParticle_HighPt__1up");
    syst_list.push_back("JET_SingleParticle_HighPt__1down");
    syst_list.push_back("JET_EffectiveNP_Modelling1__1up");
    syst_list.push_back("JET_EffectiveNP_Modelling1__1down");
    syst_list.push_back("JET_EffectiveNP_Modelling2__1up");
    syst_list.push_back("JET_EffectiveNP_Modelling2__1down");
    syst_list.push_back("JET_EffectiveNP_Modelling3__1up");
    syst_list.push_back("JET_EffectiveNP_Modelling3__1down");
    syst_list.push_back("JET_EffectiveNP_Modelling4__1up");
    syst_list.push_back("JET_EffectiveNP_Modelling4__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical1__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical1__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical2__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical2__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical3__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical3__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical4__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical4__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical5__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical5__1down");
    syst_list.push_back("JET_EffectiveNP_Statistical6__1up");
    syst_list.push_back("JET_EffectiveNP_Statistical6__1down");
    syst_list.push_back("JET_JER_EffectiveNP_12restTerm__1up");
    syst_list.push_back("JET_JER_EffectiveNP_12restTerm__1down");
    syst_list.push_back("JET_EtaIntercalibration_TotalStat__1up");
    syst_list.push_back("JET_EtaIntercalibration_TotalStat__1down");
    syst_list.push_back("JET_EtaIntercalibration_Modelling__1up");
    syst_list.push_back("JET_EtaIntercalibration_Modelling__1down");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_highE__1up");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_highE__1down");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_2018data__1up");
    syst_list.push_back("JET_EtaIntercalibration_NonClosure_2018data__1down");

    for (auto syst : syst_list)
    {     
      std::cout<<"Processing systematic uncertainty:"<<syst<<std::endl;
      TString ntuple_syst = syst;
      TTree *t1 = (TTree*)file->Get(ntuple_syst);

      vector<float>* truth_px = new vector<float>();
      vector<float>* truth_py = new vector<float>();
      vector<float>* truth_pz = new vector<float>();
      vector<float>* truth_E = new vector<float>();
      vector<float>* px = new vector<float>();
      vector<float>* py = new vector<float>();
      vector<float>* pz = new vector<float>();
      vector<float>* E = new vector<float>();

      vector<int>* pdg = new vector<int>();
      vector<int>* charge = new vector<int>(); 
      
      float truth_weight, truth_mll, truth_mlly, truth_pt_Z, truth_Ry_Z, truth_Eta_Z, weight, mll, mlly, pt_Z, Ry_Z, Eta_Z;
      int passReconstructed;
      int passFiducial;
    
      t1->SetBranchAddress("truth_px", &truth_px);
      t1->SetBranchAddress("truth_py", &truth_py);
      t1->SetBranchAddress("truth_pz", &truth_pz);
      t1->SetBranchAddress("truth_E", &truth_E);
      t1->SetBranchAddress("truth_mll", &truth_mll);
      t1->SetBranchAddress("truth_mlly", &truth_mlly);
      t1->SetBranchAddress("truth_weight", &truth_weight);
      t1->SetBranchAddress("truth_pt_Z", &truth_pt_Z);
      t1->SetBranchAddress("truth_Ry_Z", &truth_Ry_Z);   
      t1->SetBranchAddress("truth_Eta_Z", &truth_Eta_Z);    
      t1->SetBranchAddress("px", &px);
      t1->SetBranchAddress("py", &py);
      t1->SetBranchAddress("pz", &pz);
      t1->SetBranchAddress("E", &E);
      t1->SetBranchAddress("mll", &mll);
      t1->SetBranchAddress("mlly", &mlly);
      t1->SetBranchAddress("weight", &weight);
      t1->SetBranchAddress("pt_Z", &pt_Z);
      t1->SetBranchAddress("Ry_Z", &Ry_Z);
      t1->SetBranchAddress("Eta_Z", &Eta_Z); 
      t1->SetBranchAddress("pdg", &pdg);
      t1->SetBranchAddress("charge", &charge);
      t1->SetBranchAddress("passReconstructed", &passReconstructed);
      t1->SetBranchAddress("passFiducial", &passFiducial);

      int evetnum = (int)t1->GetEntries();

      TNtuple *syst_name = new TNtuple(ntuple_syst, "Theta and phi in CS frame", "truth_Ry_ll:truth_Eta_ll:truth_pt_ll:truth_ctheta_CS:truth_phi_CS:truth_weight:passFiducial:Ry_ll:Eta_ll:pt_ll:ctheta_CS:phi_CS:weight:passReconstructed");
    
      float p_x, p_y, p_z, Ez, p1_x, p1_y, p1_z, E1, p2_x, p2_y, p2_z, E2, alpha;
      float beta_z, b1x, b1y, b1z, beta_x, b2x, b2y, b2z;

      float rp_x, rp_y, rp_z, rEz, rp1_x, rp1_y, rp1_z, rE1,rp2_x, rp2_y, rp2_z, rE2, ralpha;
      float rbeta_z, rb1x, rb1y, rb1z, rbeta_x, rb2x, rb2y, rb2z;

      float reco_weight;
      int PDG, Charge;
      bool polar_range_fiducial;
      bool polar_range_reco;
      bool Ry_bin_fiducial;
      bool Ry_bin_reco;
    
      TLorentzVector p, p1, p2;
      TLorentzVector rp, rp1, rp2;
      TVector3 b1, b2, p1_vec3, p2_vec3, p1_vec3_lab, p2_vec3_lab;
      TVector3 rb1, rb2, rp1_vec3, rp2_vec3, rp_vec3, rp1_vec3_lab, rp2_vec3_lab;

      for(int m = 0;m < evetnum;m++)
      {
        t1->GetEntry(m);
      
        reco_weight = weight;
      
        polar_range_fiducial = -1;
        polar_range_reco = -1;
      
        polar_range_fiducial = truth_mll >= 80 && truth_mll <= 100 ? 1 : 0;
        polar_range_reco = mll >= 80 && mll <= 100 ? 1 : 0;
      
        Charge = passReconstructed == 1 ? charge->at(0) : 0;
        PDG = passFiducial == 1 ? pdg->at(0) : 0;
       
        // electron
        p1_x = truth_px->at(0);
        p1_y = truth_py->at(0);
        p1_z = truth_pz->at(0);
        E1 = truth_E->at(0);
        p1.SetPxPyPzE(p1_x, p1_y, p1_z, E1);
        p1_vec3_lab = p1.Vect();
        //position
        p2_x = truth_px->at(1);
        p2_y = truth_py->at(1);
        p2_z = truth_pz->at(1);
        E2 = truth_E->at(1);
        p2.SetPxPyPzE(p2_x, p2_y, p2_z, E2);

        p = p1 + p2;
        p_x = p(0);
        p_y = p(1);
        p_z = p(2);
        Ez = p(3);

        alpha = acos(p_x/sqrt(p_x*p_x+p_y*p_y));
        if(p(1) >= 0)
        {
          p.RotateZ(-alpha);
          p1.RotateZ(-alpha);
          p2.RotateZ(-alpha);
        } 
        else
        {
          p.RotateZ(alpha);
          p1.RotateZ(alpha);
          p2.RotateZ(alpha);
        }

        beta_z = -p(2)/p(3);
        if (beta_z > 0.999) beta_z = 0.999;
        if (beta_z < -0.999) beta_z = -0.999;
      
        b1x = 0;
        b1y = 0;
        b1z = beta_z;

        b1.SetXYZ(b1x, b1y, b1z);
        p.Boost(b1);
        p1.Boost(b1);
        p2.Boost(b1);

        beta_x = -p(0)/p(3);
        if(beta_x > 0.99999999) beta_x = 0.99999999;
        if(beta_x < -0.99999999) beta_x = -0.99999999;

        b2x=beta_x;
        b2y=0;
        b2z=0;

        b2.SetXYZ(b2x, b2y, b2z);
        p.Boost(b2);
        p1.Boost(b2);
        p2.Boost(b2);

        p1_vec3 = p1.Vect();
        p2_vec3 = p2.Vect();

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
        //reco
        // electron
        rp1_x = px->at(0);
        rp1_y = py->at(0);
        rp1_z = pz->at(0);
        rE1 = E->at(0);
        rp1.SetPxPyPzE(rp1_x, rp1_y, rp1_z, rE1);
      
        rp2_x = px->at(1);
        rp2_y = py->at(1);
        rp2_z = pz->at(1);
        rE2 = E->at(1);
        rp2.SetPxPyPzE(rp2_x, rp2_y, rp2_z, rE2);
      
        rp1_vec3_lab = rp1.Vect();
      
        // positron

        rp = rp1 + rp2;
        rp_x = rp(0);
        rp_y = rp(1);
        rp_z = rp(2);
        rEz = rp(3);
      
        ralpha=acos(rp_x/sqrt(rp_x*rp_x+rp_y*rp_y));
        if(rp(1)>=0)
        {
          rp.RotateZ(-ralpha);
          rp1.RotateZ(-ralpha);
          rp2.RotateZ(-ralpha);
        }
        else
        {
          rp.RotateZ(ralpha);
          rp1.RotateZ(ralpha);
          rp2.RotateZ(ralpha);
        }  

        rbeta_z = -rp(2)/rp(3);
        if(rbeta_z > 0.999) rbeta_z = 0.999;
        if(rbeta_z < -0.999) rbeta_z = -0.999;
      
        rb1x = 0;
        rb1y = 0;
        rb1z = rbeta_z;

        rb1.SetXYZ(rb1x,rb1y,rb1z);
        rp.Boost(rb1);
        rp1.Boost(rb1);
        rp2.Boost(rb1);

        rbeta_x = -rp(0)/rp(3);
        if(rbeta_x > 0.99999999) rbeta_x = 0.99999999;
        if(rbeta_x < -0.99999999) rbeta_x = -0.99999999;

        rb2x=rbeta_x;
        rb2y=0;
        rb2z=0;

        rb2.SetXYZ(rb2x,rb2y,rb2z);
        rp.Boost(rb2);
        rp1.Boost(rb2);
        rp2.Boost(rb2);

        rp_vec3 = rp.Vect();
        rp1_vec3 = rp1.Vect();
        rp2_vec3 = rp2.Vect();
      
        if (passFiducial*polar_range_fiducial == 1 || passReconstructed*polar_range_reco == 1)
        {
          syst_name->Fill(truth_Ry_Z, truth_Eta_Z, truth_pt_Z, cos(p1_vec3.Theta()), p1_vec3.Phi()+M_PI, truth_weight, passFiducial*polar_range_fiducial, Ry_Z, Eta_Z, pt_Z, cos(rp1_vec3.Theta()), rp1_vec3.Phi()+M_PI, reco_weight, passReconstructed*polar_range_reco);
        }
      }
    }
    f->Write(); 
  }
  //file->Close();
  //f->Close();
}
